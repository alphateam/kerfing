Laser cutting round boxes from square sheets
============================================

From the project brief:

> Kerf bending is a cute method for making round boxes with a laser
> cutter, but it requires quite a bit of planning. The goal of this
> project is to build a system that can be used by customers to design
> their own wooden boxes in arbitrary shapes (e.g. letters of the
> alphabet), with the templates for the round sides automatically produced
> using kerf bending. The resulting pieces should be suitable for posting
> in a flat-pack format, in as few pieces as possible, with tongue and
> slot assembly. Examples of the resulting designs will be fabricated
> using the Computer Laboratory laser cutter.

[Background on kerf bending](http://hackaday.com/2012/06/12/bending-laser-cut-wood-without-steam-or-forms/)
