/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.Constants;
import uk.ac.cam.cl.gproj.alpha.exceptions.BoxException;
import uk.ac.cam.cl.gproj.alpha.interfaces.SheetContext;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Joins two surfaces via finger joints, overlapping them a bit to account for material lost to the laser and to make
 * the joint stronger.
 */
public class FingerJointGenerator implements JointGenerator, Constants {

    private int mOverlap;

    /**
     * Default constructor with overlap = 3;
     */
    public FingerJointGenerator() {
        this(3);
    }

    /**
     * Constructor specifying the overlap between fingers
     *
     * @param overlap How much to overlap the fingers to make up for the material lost to the laser and to make sure the
     *                joint is strong. In tenths (0.1) of millimeters, as usual.
     */
    public FingerJointGenerator(int overlap) {
        mOverlap = overlap;
    }

    /**
     * See JointGenerator
     */
    public String getName() {
        return "Finger joint";
    }

    /**
     * Calculates the (unsigned) length of a Line2D
     *
     * @param line The line whose length we want to know
     * @return The length of the line
     */
    static private double lineLength(Line2D line) {
        return Math.abs(line.getP1().distance(line.getP2()));
    }

    /**
     * Return a vector that you can add to line.P1 to get to distance along line.
     *
     * @param distance Distance
     * @param line Line
     * @return V such that line.P1 + V is distance along the line
     */
    static private Point2D distanceAlongLine(double distance, Line2D line) {
        // (P2 - P1) * (distance / line length)
        return mult(sub(line.getP2(), line.getP1()), distance / lineLength(line));
    }

    /**
     * Return a vector that you can add to any point on line to get a point on the line parallel with line at distance
     * distance in the direction side (direction is true if Line2D.relativeCCW returns 1, false for -1).
     *
     * @param distance Distance
     * @param direction Direction/side of the line
     * @param line Line
     * @return Vector as described above
     */
    static private Point2D distanceFromLine(double distance, boolean direction, Line2D line) {
        Point2D p = distanceAlongLine(distance, line);
        if (direction) {
            p.setLocation(p.getY(), -p.getX());
        } else {
            p.setLocation(-p.getY(), p.getX());
        }
        return p;
    }

    /**
     * Add 2 points/position vectors.
     *
     * @param a Operand 1
     * @param b Operand 2
     * @return The sum of the operands
     */
    static private Point2D add(Point2D a, Point2D b) {
        return new Point2D.Double(a.getX() + b.getX(), a.getY() + b.getY());
    }

    /**
     * Subtract a point/position vector from another.
     *
     * @param a A
     * @param b B
     * @return A - B
     */
    static private Point2D sub(Point2D a, Point2D b) {
        return new Point2D.Double(a.getX() - b.getX(), a.getY() - b.getY());
    }

    /**
     * Multiply a point/position vector by a scalar
     *
     * @param a A
     * @param b b
     * @return A * b
     */
    static private Point2D mult(Point2D a, double b) {
        return new Point2D.Double(a.getX() * b, a.getY() * b);
    }

    /**
     * Draws a finger joint. The fingers will be of height and width height.
     *
     * See JointGenerator
     */
    // For List<Point2D>[] path = new List[2]. We can't have generic arrays and ArrayList would just make the code ugly
    @SuppressWarnings("unchecked")
    public void drawJoint(SheetContext contextA, Line2D Av, boolean Ad,
                          SheetContext contextB, Line2D Bv, boolean Bd,
                          int height) {
        // Check that the rectangles are congruent
        if (Math.abs(lineLength(Av) - lineLength(Bv)) > EPS) {
            throw new BoxException("Jointing rectangles not congruent");
        }

        // Determine how many fingers we need
        int fingers = (int)(lineLength(Av) / height);

        // Useful constant
        Point2D zero = new Point2D.Double(0,0);

        // Finger dimensions
        Point2D[] along = {
            distanceAlongLine(lineLength(Av) / fingers, Av),
            distanceAlongLine(lineLength(Bv) / fingers, Bv)
        };

        Point2D[] overlap = {
            distanceAlongLine(mOverlap, Av),
            distanceAlongLine(mOverlap, Bv)
        };

        Point2D[] across = {
            distanceFromLine(height, Ad, Av),
            distanceFromLine(height, Bd, Bv)
        };

        // Current positions
        Point2D[] position = {
            Av.getP1(),
            Bv.getP1()
        };

        // Calculate the paths
        List<Point2D>[] path = new List[2];
        path[0] = new ArrayList<Point2D>();
        path[1] = new ArrayList<Point2D>();
        path[0].add(position[0]);
        path[1].add(position[1]);

        for (int side = 0, i = 0; i < fingers; ++i) {
            // If there are an odd number of fingers don't increase the last finger for overlap
            Point2D[] currentOverlap = overlap;
            if (fingers % 2 == 1 && i == fingers - 1) {
                currentOverlap = new Point2D[]{ zero, zero };
            }

            // Draw finger
            path[side].add(position[side] = add(position[side], across[side]));
            path[side].add(position[side] = add(position[side], add(along[side], currentOverlap[side])));
            path[side].add(position[side] = sub(position[side], across[side]));

            // Switch sides
            side ^= 1;

            // Draw line
            path[side].add(position[side] = add(position[side], sub(along[side], currentOverlap[side])));
        }

        // Draw the paths
        SheetContext[] contexts = {
            contextA,
            contextB
        };
        for (int side = 0; side < 2; ++side) {
            for (int i = 0; i < path[side].size()-1; ++i) {
                contexts[side].drawLine((int)(Math.round(path[side].get(i).getX())),
                                        (int)(Math.round(path[side].get(i).getY())),
                                        (int)(Math.round(path[side].get(i+1).getX())),
                                        (int)(Math.round(path[side].get(i+1).getY())));
            }
        }
    }

}
