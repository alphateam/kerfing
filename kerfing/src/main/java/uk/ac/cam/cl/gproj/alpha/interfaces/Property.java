/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;

/**
 * <p>
 *     Describes a configurable property of a box.
 * </p>
 *
 * <p>
 *     Hidden if the value is null.
 * </p>
 *
 * @param <T> The type of the property
 */
public abstract class Property<T> {

    protected T mValue;
    protected String mName;

    /**
     * Constructs a new property with the specified name
     *
     * @param name The property's name
     */
    public Property(String name) {
        mName = name;
    }

    /**
     * Gets the value of the property
     *
     * @return The value
     */
    public T getValue() {
        return mValue;
    }

    /**
     * Sets the value of the property
     *
     * @param aValue The new value
     */
    public void setValue(T aValue) throws InvalidArgumentException {
        mValue = aValue;
    }

    /**
     * Gets the name of the property
     *
     * @return Textual description of property
     */
    public String getName() {
        return mName;
    }

}
