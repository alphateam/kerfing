/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

import uk.ac.cam.cl.gproj.alpha.exceptions.NoACIException;

import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.util.HashMap;
import java.util.Map;

import org.apache.batik.ext.awt.g2d.GraphicContext;

/**
 * The purpose of DXFShape is to create a map of a shape's properties
 * (entitiyMap) in terms of DXF keys and values.
 */
public class DXFShape implements DXFConstants {
    /** This shape's DXF type (ARC or LINE at the moment) */
    String type;
    /** This shape's DXF properties (endpoints, colour etc.) */
    Map<Integer,String> entityMap;

    /**
     * DXF uses an upside-down coordinate system: y grows upwards
     * instead of downwards. This helper function inverts its argument
     * and then converts it into a String.
     *
     * @param   d   the number to invert and convert to String
     */
    private static String invertAndConvert (double d) {
        return convert(-d);
    }

    /**
     * This helper function converts its argument into a String.
     *
     * @param   d   the number to convert to String
     */
    private static String convert (double d) {
        return Double.toString(d);
    }

    /**
     * Construct a DXFShape from a java.awt.Shape and a GraphicsContext
     * giving its colour.
     *
     * This method outputs an error method to System.err if the Shape
     * type is not supported, but does not raise an exception.
     *
     * @param   shape   the java.awt.Shape to represent
     * @param   gc      the current graphics context (contains the
     *                  current stroke colour)
     */
    public DXFShape(Shape shape, GraphicContext gc) {
        entityMap = new HashMap<Integer,String>();
        int aciColor = 0;
        try {
            aciColor = DXFColorConverter.colorToAci(gc.getColor());
        } catch (NoACIException e) {
            System.err.println(e.getMessage());
        }
        entityMap.put(DXF_COLOR, Integer.toString(aciColor));

        if (shape instanceof Line2D)
            putLine((Line2D)shape);
        else if (shape instanceof Arc2D)
            putArc((Arc2D)shape);
        else
            System.err.println("DXFShape: Unsupported shape type");
    }

    /**
     * Make this DXFShape represent a line.
     *
     * Sets this DXFShape's type to LINE, and adds the line's end points
     * to the entityMap.
     *
     * @param   l2d   the Line2D to represent
     */
    protected void putLine(Line2D l2d) {
        type = DXF_LINE;
        entityMap.put(DXF_LINE_START_X, convert(l2d.getX1()));
        entityMap.put(DXF_LINE_START_Y, invertAndConvert(l2d.getY1()));
        entityMap.put(DXF_LINE_END_X, convert(l2d.getX2()));
        entityMap.put(DXF_LINE_END_Y, invertAndConvert(l2d.getY2()));
    }

    /**
     * Make this DXFShape represent an arc.
     *
     * Sets this DXFShape's type to ARC, and updates the entityMap
     * according to the points and arcs defined in the argument.
     *
     * @param   a2d   the Arc2D to represent
     */
    protected void putArc(Arc2D a2d) {
        type = DXF_ARC;
        entityMap.put(DXF_ARC_CENTER_X, convert(a2d.getCenterX()));
        entityMap.put(DXF_ARC_CENTER_Y, invertAndConvert(a2d.getCenterY()));
        // This only works for circular arcs
        entityMap.put(DXF_ARC_RADIUS,
                convert(a2d.getWidth() / 2.0d));
        entityMap.put(DXF_ARC_ANGLE_START, convert(a2d.getAngleStart()));
        entityMap.put(DXF_ARC_ANGLE_END,
                convert(a2d.getAngleStart() + a2d.getAngleExtent()));
    }
}
