/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;
import uk.ac.cam.cl.gproj.alpha.interfaces.Box;
import uk.ac.cam.cl.gproj.alpha.interfaces.BoxHelper;
import uk.ac.cam.cl.gproj.alpha.interfaces.ChoiceProperty;
import uk.ac.cam.cl.gproj.alpha.interfaces.GenericProperty;
import uk.ac.cam.cl.gproj.alpha.interfaces.Property;
import uk.ac.cam.cl.gproj.alpha.interfaces.RangedProperty;


public class ShapeCard extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1L;
    private final WizardCard parent;
    private final Box[] boxes = BoxHelper.getBoxes();
    private final Map<Object, Icon> shapeIcons = new HashMap<Object, Icon>();
    private final JComboBox combo;
    private JPanel propertiesPanel = new JPanel();
    private final JButton next = new JButton("next");
    private final JButton back = new JButton("back");
    private final JLabel status = new JLabel("status:");
    private final JLabel info = new JLabel("         please give a valid input      ");

    private Box box;

    public ShapeCard (WizardCard wc) {
        this.parent = wc;
        setLayout(new BorderLayout());

        // set up mappings for which icon to use for each value
        for (Box b: boxes) {
            shapeIcons.put(b.getName(), new ImageIcon(b.getIcon()));
        }
        // In the future, for each Box type, make a new mapping from its name to its Icon

        combo = new JComboBox(shapeIcons.keySet().toArray());
        //setBox(boxes[combo.getSelectedIndex()]);
        //parent.setBox(box);

        // create a cell renderer to add the appropriate icon
        combo.setRenderer(new IconListRenderer(shapeIcons));

        combo.addActionListener(this);
        next.addActionListener(this);
        back.addActionListener(this);


        JPanel selectShapePanel = new JPanel();
        selectShapePanel.add(new JLabel("Select a box shape: "));
        selectShapePanel.add(combo);
        selectShapePanel.add(status);
        selectShapePanel.add(info);

        JPanel navigationPanel = new JPanel(new BorderLayout());
        navigationPanel.add(back, BorderLayout.WEST);
        navigationPanel.add(next, BorderLayout.EAST);

        add(selectShapePanel, BorderLayout.NORTH);
        add(navigationPanel, BorderLayout.SOUTH);
    }

    public void initBox() {
        parent.setBox(boxes[combo.getSelectedIndex()]);
    }


    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == combo) {
            JComboBox cb = (JComboBox)e.getSource();
            int shapeIndex = cb.getSelectedIndex();
            parent.setBox(boxes[shapeIndex]);
        } else if(e.getSource() == next) {
            ((CardLayout) parent.getLayout()).next(this.getParent());

        } else if(e.getSource() == back) {
            ((CardLayout) parent.getLayout()).previous(this.getParent());
        } 
    }

    public void setBox(Box box) {
        this.box = box;
        if (propertiesPanel != null) {
            this.remove(propertiesPanel);
        }
        propertiesPanel = new JPanel(new GridLayout(0, 2));

        List<Property> propList = box.getPropertyList();
        for (final Property p: propList) {
            if (p.getName().equals("Kerf Pattern")) { continue; }

            JLabel nameLabel = new JLabel(p.getName());
            propertiesPanel.add(nameLabel);

            if (p instanceof ChoiceProperty) {
                ChoiceProperty choiceProp = (ChoiceProperty)p;
                for (Object c: choiceProp.getChoiceList()) {
                    c.toString();
                }
            } else if (p instanceof RangedProperty) {
                RangedProperty rangeProp = ((RangedProperty)p);
                int min = 0, max = 0, init = 0;
                if (p.getValue() instanceof Integer) {
                    min = (Integer) rangeProp.getMinimum();
                    max = (Integer) rangeProp.getMaximum();
                    init = (Integer) rangeProp.getValue();
                } else if (p.getValue() instanceof Double) {
                    min = (int) Math.round((Double) rangeProp.getMinimum());
                    max = (int) Math.round((Double) rangeProp.getMaximum());
                    init = (int) Math.round((Double) rangeProp.getValue());
                }
                final JSlider rangeSlider = new JSlider(JSlider.HORIZONTAL, min, max, init);
                rangeSlider.setMajorTickSpacing((int) Math.round((max-min)/5.0));
                rangeSlider.setPaintTicks(true);
                rangeSlider.setPaintLabels(true);

                final JTextField rangeSliderField = new JTextField(10);
                rangeSliderField.setText(Integer.toString(init));

                rangeSlider.addChangeListener(new ChangeListener(){
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        int propValue = rangeSlider.getValue();
                        rangeSliderField.setText(String.valueOf(propValue));
                        try {
                            // Sets the value of the RangedProperty
                            if (p.getValue() instanceof Double) {
                                p.setValue(propValue*1.0);
                            } else {
                                p.setValue(propValue);
                            }
                        } catch (InvalidArgumentException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
                
                final Pattern NUM_PATTERN = Pattern.compile("\\d*");
                rangeSliderField.addKeyListener(new KeyAdapter(){
                    @Override
                    public void keyReleased(KeyEvent ke) {

                        String typed = rangeSliderField.getText();
                        Matcher m = NUM_PATTERN.matcher(typed);
                        if (!m.matches()) {
                            getToolkit().beep();
                            ke.consume();
                            Object[] options = { "OK" };
                            JOptionPane.showOptionDialog(null, "only number allowed!", "Warning",
                                    JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                                    null, options, options[0]);
                            typed = typed.substring(0, typed.length() - 1);
                            rangeSliderField.setText(typed);
                        }

                        rangeSlider.setValue(0);
                        if(!typed.matches("\\d+") || typed.length() > 3) {
                            return;
                        }

                        int value = Integer.parseInt(typed);
                        if ( Integer.parseInt(rangeSliderField.getText())<(rangeSlider.getMinimum())){
                            info.setText("invalid input and please continue");
                        }else {
                            info.setText("         you input a valid value        ");

                        }
                        rangeSlider.setValue(value);
                    }
                });



                JPanel rangeSliderPanel = new JPanel(new BorderLayout());
                rangeSliderPanel.add(rangeSlider, BorderLayout.CENTER);
                rangeSliderPanel.add(rangeSliderField, BorderLayout.EAST);
                propertiesPanel.add(rangeSliderPanel, BorderLayout.CENTER);

                try {
                    Object o = p.getValue();
                    if (o instanceof Integer) {
                        p.setValue(rangeSlider.getValue());
                    }
                    else if (o instanceof Double) {
                        p.setValue(new Double(rangeSlider.getValue()));
                    }
                } catch (InvalidArgumentException e1) {
                    System.err.println("RangedProperty value is out of allowed range.");
                }
            } else {
                GenericProperty rangeProp = ((GenericProperty)p);
                propertiesPanel.add(new JTextField(), BorderLayout.EAST);
            }
        }

        this.add(propertiesPanel, BorderLayout.CENTER);
        this.validate();
    }

}
