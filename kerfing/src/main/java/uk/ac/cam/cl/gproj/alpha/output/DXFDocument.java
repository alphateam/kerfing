/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Represents a DXF document. We can add entities to it and stream
 * it to an output stream.
 */
public class DXFDocument implements DXFConstants {
    StringBuffer buffer;

    /**
     * Empty default constructor.
     */
    public DXFDocument() {
        buffer = new StringBuffer();
        buffer.append(DXF_INTRO);
    }

    /**
     * Add an entity to the DXF document.
     *
     * @param   shape   the shape of the entity to be added.
     */
    public void addEntity(DXFShape shape) {
        // buffer.append(DXF_NEWLINE);
        buffer.append(DXF_ENTITY);
        buffer.append(DXF_NEWLINE);
        buffer.append("  " + shape.type);
        buffer.append(DXF_NEWLINE);
        
        Iterator<Map.Entry<Integer,String> > iter =
                shape.entityMap.entrySet().iterator();
        while (iter.hasNext()){
            Map.Entry<Integer,String> entry =
                    (Map.Entry<Integer,String>)iter.next();
            buffer.append(entry.getKey().toString());
            buffer.append(DXF_NEWLINE);
            buffer.append("  " + entry.getValue().toString());
            buffer.append(DXF_NEWLINE);
        }
    }

    /**
     * Write this DXF document into an output stream.
     *
     * @param   out   the output stream into which we stream the
     *                document
     */
    public void writeTo(OutputStream out) throws IOException {
        out.write(buffer.toString().getBytes());
        out.write(DXF_OUTRO.getBytes());
    }
}
