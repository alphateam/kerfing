/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.interfaces.SheetContext;

import java.awt.geom.Line2D;

/**
 * Defines an object that can draw joints for 2 surfaces
 */
public interface JointGenerator {

    /**
     * Gives a name to the generator.
     *
     * @return A name for the generator.
     */
    public String getName();

    /**
     * Draws cuts in the 2 contexts such that the pieces that they represent can be joined on the specified
     * <b>congruent</b> rectangles.
     *
     * <p>
     * A rectangle is specified by 2 perpendicular vectors, representing 2 edges, the first parallel and the second
     * perpendicular to (and pointing to) the common edge of the join. The first vector is given by its endpoints and
     * the second one by its magnitude - common to both rectangles - and direction, specified as a boolean according to
     * the return value of Line2D.relativeCCW(double, double, double, double, double, double): true for positive values,
     * false for negative values. Assuming x grows rightwards and y upwards, true is to the right of the vector, false
     * to the left.
     * </p>
     *
     * <p>
     * The direction of the first (parallel) vectors also specify in which direction the pieces are joined together.
     * The joint goes along Av and Bv so when the pieces are joined Av1 + (Av2 - Av1) * d would correspond to
     * Bv1 + (Bv2 - Bv1) * d, for d scalar.
     * </p>
     *
     * <p>
     * This method will throw an unchecked BoxException if the rectangles are not congruent.
     * </p>
     *
     * @param contextA The SheetContext for the first piece.
     * @param Av The first vector for A
     * @param Ad The direction of the second vector for A
     * @param contextB The SheetContext for the second piece.
     * @param Bv The first vector for B
     * @param Bd The direction of the second vector for B
     * @param height The magnitude of the second vector, common for A and B
     */
    public void drawJoint(SheetContext contextA, Line2D Av, boolean Ad,
                          SheetContext contextB, Line2D Bv, boolean Bd,
                          int height);
}
