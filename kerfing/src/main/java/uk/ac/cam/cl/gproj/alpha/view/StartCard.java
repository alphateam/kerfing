/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StartCard extends JPanel{
    private static final long serialVersionUID = 1L;
    
    private final JLabel logoLabel;
    private final JButton createButton = new JButton("Start new project");
    private final GUI mygui;
    
    public StartCard(final GUI mygui) {
        this.mygui = mygui;
        
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        try {
            logoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("resources/start.jpeg"))));
        } catch (IOException e) {           
            throw new RuntimeException("Logo file not found.", e);                 
        }
        
        for (Component c: getComponents()){
            GUI.makeNonResizeable(c);
        }
        
        BorderLayout layout = new BorderLayout();
        setLayout(layout);
        add(logoLabel, BorderLayout.CENTER);
        add(createButton, BorderLayout.SOUTH);
        
        final StartCard sc = this;
        createButton.addActionListener(new ActionListener() {    
            @Override
            public void actionPerformed(ActionEvent ae) {
                mygui.getLayout().next(sc.getParent());
                
            }
        });
    }
    
    
}
