/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * <p>
 *     A type Box's can inherit from to get kerf patterns. 
 * </p>
 */
public abstract class StandardBox implements Box {

    private final List<KerfGenerator> mKerfGenerators = Arrays.<KerfGenerator>asList(new SimpleKerfGenerator(), new SpaceInvaderKerfGenerator());
    protected final ChoiceProperty<KerfGenerator> mKerfGeneratorProperty = new ChoiceProperty<KerfGenerator>("Kerf Pattern") {
            {
                try {
                    setValue(0); //set default value        
                }
                catch (InvalidArgumentException e) {
                    //no default to pick -> okay for now, but box won't process if it needs a kerf generator
                }
            }
            @Override
            public List<KerfGenerator> getChoiceList() {
                return mKerfGenerators;
            }
        };

    protected final List<Property> Properties = new ArrayList<Property>();

    public StandardBox() {
        Properties.add(mKerfGeneratorProperty);
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract String getIcon();

    public List<Property> getPropertyList() {
        return Properties;
    }

    /**
     * Gets the selected Kerf Generator if a single one was selected, else returns null. 
     */
    protected KerfGenerator getKerfGenerator() {
        TreeSet<Integer> ts = mKerfGeneratorProperty.getValue();
        if (ts.size() == 1) {
            return mKerfGenerators.get(ts.first());
        }
        else {
            return null;
        }
    }

    public abstract void process(GraphicsPrinter gp);

}
