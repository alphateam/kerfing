/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class JointsCard extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final WizardCard parent;
	private final GridBagLayout layout = new GridBagLayout();
	private final GridBagConstraints gridBagConstraints = new GridBagConstraints();
	private final ButtonGroup jointChoiceGroup = new ButtonGroup ();
	private final JRadioButton mtjointButton = new JRadioButton ("Mortise and tenon joint");
	private final JRadioButton fingerJoints = new JRadioButton ("Finger joints");
	private final JLabel jointIconLabel = new JLabel (new ImageIcon("resources/joint-mortiseandtenon.png"));
	private final JPanel choicePanel = new JPanel();
	private final JPanel jointIconPanel = new JPanel(new BorderLayout());
	private final JButton back = new JButton("back");
	private final JButton next = new JButton("next");

	public JointsCard(WizardCard wc) {
		this.parent=wc;
		this.setLayout(layout);
		gridBagConstraints.fill = GridBagConstraints.BOTH;	
		
	    setGridBagConstriants(0, 0, 2, 1, 0, 1.0);
		this.add(choicePanel, gridBagConstraints);
		
		jointIconPanel.add(jointIconLabel,BorderLayout.CENTER);
		setGridBagConstriants(2, 0, 2, 1, 1.0, 1.0);
		this.add(jointIconPanel, gridBagConstraints);
		
		setGridBagConstriants(0, 1, 1, 1, 0, 0);
		this.add(back,gridBagConstraints);
        back.addActionListener(this);

        setGridBagConstriants(3, 1, 1, 1, 0, 0);
		this.add(next,gridBagConstraints);
		next.addActionListener(this);
		
		choicePanel.setLayout(new GridLayout(3,1));
		choicePanel.setBorder(BorderFactory.createTitledBorder(""));
		jointIconPanel.setBorder(BorderFactory.createTitledBorder(""));

		mtjointButton.addActionListener(this);
		fingerJoints.addActionListener(this);

		jointChoiceGroup.add (fingerJoints);
		jointChoiceGroup.add (mtjointButton);


		
		choicePanel.add(fingerJoints);
		choicePanel.add(mtjointButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mtjointButton) {
			jointIconLabel.setIcon(new ImageIcon("resources/joint-mortiseandtenon.png"));
		}else if(e.getSource()== fingerJoints){
			jointIconLabel.setIcon(new ImageIcon("resources/joint-finger.png"));
		}else if(e.getSource()== back) {
			((CardLayout) parent.getLayout()).previous(this.getParent());
		}else if(e.getSource()== next){
			((CardLayout) parent.getLayout()).next(this.getParent());
		}
	}
	public void setGridBagConstriants(int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty){
		gridBagConstraints.gridx = gridx;
		gridBagConstraints.gridy = gridy;
		gridBagConstraints.gridwidth = gridwidth;
		gridBagConstraints.gridheight = gridheight;
		gridBagConstraints.weightx = weightx;
		gridBagConstraints.weighty = weighty;
	}

}
