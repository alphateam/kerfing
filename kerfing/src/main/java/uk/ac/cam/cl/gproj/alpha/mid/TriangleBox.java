package uk.ac.cam.cl.gproj.alpha.mid;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;
import java.awt.Rectangle;
import java.awt.geom.Line2D;

/**
 * <p>
 *     Represents a box that is a cuboid with curved corners when looked down upon it. 
 * </p>
 */
public class TriangleBox extends StandardBox {

    protected static final String NAME = "Triangle Box"; 
    protected static final String DESCRIPTION = "A triangular prism shaped box. "; 
    protected static final String ICON_RESOURCE = "resources/cube.png"; //not an up to date value

    //Property definitions. Values are in mm for ease of use to the 
    //user (they are converted to 0.1mm units at start of process call)
    private final Property<Integer> mLeftWidthProperty = new RangedProperty<Integer>("Width of Left Side (mm)") {
            {
                mMaximum = 400;
                mValue = 35;
                mMinimum = 35;
            }
        };
    private final Property<Integer> mRightWidthProperty = new RangedProperty<Integer>("Width of Right Side (mm)") {
            {
                mMaximum = 400;
                mValue = 75;
                mMinimum = 35;
            }
        };
    private final Property<Integer> mHeightProperty = new RangedProperty<Integer>("Height (mm)") {
            {
                mMaximum = 400;
                mValue = 50;
                mMinimum = 50;
            }
        };
    private final Property<Integer> mDepthProperty = new RangedProperty<Integer>("Depth (mm)") {
            {
                mMaximum = 400;
                mValue = 50;
                mMinimum = 10;
            }
        };
    private final Property<Integer> mKerfColorsProperty = new RangedProperty<Integer>("Number of colors for kerf patterns") {
            {
                mMaximum = 6;
                mValue = 2;
                mMinimum = 1;
            }
        };
    private final Property<Double> mWoodThicknessProperty = new RangedProperty<Double>("Thickness of Wood (mm)") {
            {
                mMaximum = 10.0;
                mValue = 3.0;
                mMinimum = 0.0; //let the user select 0 if they want to remove wood thickness error cancelling
            }
        };

    public String getName() {
        return NAME;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public String getIcon() {
        return ICON_RESOURCE;
    }

    public TriangleBox() 
    {
        Properties.add(mLeftWidthProperty);
        Properties.add(mRightWidthProperty);
        Properties.add(mHeightProperty);
        Properties.add(mDepthProperty);
        Properties.add(mKerfColorsProperty);
        Properties.add(mWoodThicknessProperty);
    }

    public void process(GraphicsPrinter graphicsPrinter) {
        //basic values
        double height = mHeightProperty.getValue() * 10;
        double w1 = mLeftWidthProperty.getValue() * 10;
        double w2 = mRightWidthProperty.getValue() * 10;
        double cornerCutLength = 200;
        int jointBuffer = 50;
        double depth = mDepthProperty.getValue() * 10;
        int kerfColors = mKerfColorsProperty.getValue();
        double woodThickness = mWoodThicknessProperty.getValue() * 10;

        //computed values
        double width = w1 + w2;
        double baseLength = width - 2 * cornerCutLength;
        double leftSideLength = (int)Math.hypot((double)w1, (double)height) - 2 * cornerCutLength;
        double rightSideLength = (int)Math.hypot((double)w2, (double)height) - 2 * cornerCutLength;

        double rightCornerAngle = Math.atan(height / w2);
        double leftCornerAngle = Math.atan(height / w1);
        double topCornerAngle = Math.PI - leftCornerAngle - rightCornerAngle;

        double rightArcAngle = Math.PI - rightCornerAngle;
        double topArcAngle = Math.PI - topCornerAngle;
        double leftArcAngle = Math.PI - leftCornerAngle;

        double h1 = cornerCutLength * Math.tan(rightCornerAngle / 2.0);
        double h2 = cornerCutLength * Math.tan(topCornerAngle / 2.0);
        double h3 = cornerCutLength * Math.tan(leftCornerAngle / 2.0);

        double rightArcLength = (h1 + woodThickness / 2.0) * rightArcAngle;
        double topArcLength = (h2 + woodThickness / 2.0) * topArcAngle;
        double leftArcLength = (h3 + woodThickness / 2.0) * leftArcAngle;

        double length = baseLength + rightSideLength + leftSideLength + 
                        rightArcLength + topArcLength + leftArcLength;

        //precompute int casts
        int iLength = (int)Math.round(length);
        int iDepth= (int)Math.round(depth);
        int iWidth = (int)Math.round(width);
        int iHeight= (int)Math.round(height);
        int iW1 = (int)Math.round(w1);
        int iW2= (int)Math.round(w2);
        int ubWoodThickness = (int)Math.ceil(woodThickness);

        int rightArcAngleDegrees = (int)Math.round(180 * rightArcAngle / Math.PI);
        int topArcAngleDegrees = (int)Math.round(180 * topArcAngle / Math.PI);
        int leftArcAngleDegrees = (int)Math.round(180 * leftArcAngle / Math.PI);

        // Start a sheet
        Sheet sheet = graphicsPrinter.getNewSheet();
  
        // Set the size of the sheet
        sheet.setBoundingShape(new Rectangle(-ubWoodThickness, 0, iLength, iDepth));

        // Get kerf contexts to draw to
        ArrayList<SheetContext> kerfContexts = new ArrayList<SheetContext>();
        for (int i = 0; i < kerfColors; ++i) {
            kerfContexts.add(sheet.getNewContext());
        }
  
        KerfGenerator kerfer = getKerfGenerator();
        JointGenerator jointer = new FingerJointGenerator();

        double loc = baseLength / 2;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + rightArcLength, depth, false, 1.0 / (h1 + woodThickness));
        loc += rightArcLength + rightSideLength;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + topArcLength, depth, false, 1.0 / (h2 + woodThickness));
        loc += topArcLength + leftSideLength;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + leftArcLength, depth, false, 1.0 / (h3 + woodThickness));
  
        // Get last context to draw to
        SheetContext lastContext = sheet.getNewContext();
  
        //left around joint
        lastContext.drawLine(0, 0, 0, jointBuffer); 
        lastContext.drawLine(0, iDepth - jointBuffer, 0, iDepth); 
        //right around joint
        lastContext.drawLine(iLength, 0, iLength, jointBuffer); 
        lastContext.drawLine(iLength, jointBuffer, iLength - ubWoodThickness, jointBuffer); 
        lastContext.drawLine(iLength, iDepth, iLength, iDepth - jointBuffer); 
        lastContext.drawLine(iLength, iDepth - jointBuffer, iLength - ubWoodThickness, iDepth - jointBuffer); 

        //draw left-right jointing lines
        jointer.drawJoint(  lastContext, new Line2D.Double(0, jointBuffer, 0, depth - jointBuffer), false,
                            lastContext, new Line2D.Double(length - ubWoodThickness, jointBuffer, length - ubWoodThickness, depth - jointBuffer), true,
                            ubWoodThickness);

        //Print sides of box
        Sheet front = graphicsPrinter.getNewSheet();
        Sheet back = graphicsPrinter.getNewSheet();
        front.setBoundingShape(new Rectangle(0, 0, iWidth, iHeight));
        back.setBoundingShape(new Rectangle(0, 0, iWidth, iHeight));
        SheetContext frontContext = front.getNewContext();
        SheetContext backContext = back.getNewContext();
        List<SheetContext> sideContexts = Arrays.<SheetContext>asList(frontContext, backContext);

        //horrible vector math
        double x1 = width, y1 = height;
        double x2 = w1, y2 = 0;
        double x3 = 0, y3 = height;

        double v12x = x2 - x1;
        double v12y = y2 - y1;
        double v12l = Math.sqrt(v12x*v12x + v12y*v12y);
        v12x = v12x / v12l;
        v12y = v12y / v12l;

        double v23x = x3 - x2;
        double v23y = y3 - y2;
        double v23l = Math.sqrt(v23x*v23x + v23y*v23y);
        v23x = v23x / v23l;
        v23y = v23y / v23l;

        //calculate centre of arc for top of triangle
        int sa = (int)Math.round(180 * (Math.PI / 2.0 - rightCornerAngle) / Math.PI);
        double xc2 = cornerCutLength * Math.cos(rightCornerAngle) - h2 * Math.sin(rightCornerAngle);
        double yc2 = cornerCutLength * Math.sin(rightCornerAngle) + h2 * Math.cos(rightCornerAngle);

        for (SheetContext context : sideContexts)
        {
            //draw the straight edges

            //right
            context.drawLine(
                (int)Math.round(width + v12x*cornerCutLength), 
                (int)Math.round(height + v12y*cornerCutLength), 
                (int)Math.round(width + v12x*(cornerCutLength + jointBuffer)), 
                (int)Math.round(height + v12y*(cornerCutLength + jointBuffer)));

            context.drawLine(
                (int)Math.round(w1 - v12x*(cornerCutLength + jointBuffer)), 
                (int)Math.round(0 - v12y*(cornerCutLength + jointBuffer)), 
                (int)Math.round(w1 - v12x*cornerCutLength), 
                (int)Math.round(0 - v12y*cornerCutLength));

            //left
            context.drawLine(
                (int)Math.round(w1 + v23x*cornerCutLength), 
                (int)Math.round(0 + v23y*cornerCutLength), 
                (int)Math.round(w1 + v23x*(cornerCutLength + jointBuffer)), 
                (int)Math.round(0 + v23y*(cornerCutLength + jointBuffer)));

            context.drawLine(
                (int)Math.round(0 - v23x*(cornerCutLength + jointBuffer)), 
                (int)Math.round(height - v23y*(cornerCutLength + jointBuffer)), 
                (int)Math.round(0 - v23x*cornerCutLength), 
                (int)Math.round(height - v23y*cornerCutLength));

            //base
            context.drawLine(
                (int)Math.round(cornerCutLength), 
                (int)Math.round(height), 
                (int)Math.round(cornerCutLength + jointBuffer), 
                (int)Math.round(height));

            context.drawLine(
                (int)Math.round(width - cornerCutLength - jointBuffer), 
                (int)Math.round(height), 
                (int)Math.round(width - cornerCutLength), 
                (int)Math.round(height));

            //draw corners
            context.drawArc(
                (int)Math.round(width - cornerCutLength - h1), (int)Math.round(height - 2*h1), 
                (int)Math.round(h1*2), (int)Math.round(h1*2), 270, rightArcAngleDegrees);
            context.drawArc(
                (int)Math.round(w1 + xc2 - h2), (int)Math.round(yc2 - h2), 
                (int)Math.round(h2*2), (int)Math.round(h2*2), sa, topArcAngleDegrees);
            context.drawArc(
                (int)Math.round(cornerCutLength - h3), (int)Math.round(height - 2*h3), 
                (int)Math.round(h3*2), (int)Math.round(h3*2), 270 - leftArcAngleDegrees, leftArcAngleDegrees);
        }

        Line2D baseRightStraightJointLower = new Line2D.Double(jointBuffer, 
                                        depth, 
                                        baseLength / 2.0 - jointBuffer, 
                                        depth);
        Line2D baseRightStraightJointUpper = new Line2D.Double(baseRightStraightJointLower.getP1().getX(), 
                                        0, 
                                        baseRightStraightJointLower.getP2().getX(), 
                                        0);
        Line2D rightStraightJointLower = new Line2D.Double(baseLength / 2.0 + rightArcLength +  jointBuffer, 
                                        depth, 
                                        baseLength / 2.0 + rightArcLength + rightSideLength - jointBuffer, 
                                        depth);
        Line2D rightStraightJointUpper = new Line2D.Double(rightStraightJointLower.getP1().getX(), 
                                        0, 
                                        rightStraightJointLower.getP2().getX(), 
                                        0);
        Line2D leftStraightJointLower = new Line2D.Double(baseLength / 2.0 + rightArcLength + rightSideLength + topArcLength + jointBuffer, 
                                        depth, 
                                        baseLength / 2.0 + rightArcLength + rightSideLength + topArcLength + leftSideLength - jointBuffer, 
                                        depth);
        Line2D leftStraightJointUpper = new Line2D.Double(leftStraightJointLower.getP1().getX(), 
                                        0, 
                                        leftStraightJointLower.getP2().getX(), 
                                        0);
        Line2D baseLeftStraightJointLower = new Line2D.Double(baseLength / 2.0 + rightArcLength + rightSideLength + topArcLength + leftSideLength + leftArcLength + jointBuffer, 
                                        depth, 
                                        baseLength + rightArcLength + rightSideLength + topArcLength + leftSideLength + leftArcLength - jointBuffer, 
                                        depth);
        Line2D baseLeftStraightJointUpper = new Line2D.Double(baseLeftStraightJointLower.getP1().getX(), 
                                        0, 
                                        baseLeftStraightJointLower.getP2().getX(), 
                                        0);
        double baseRightJointLength = baseRightStraightJointLower.getP1().distance(baseRightStraightJointLower.getP2());
        double rightJointLength = rightStraightJointLower.getP1().distance(rightStraightJointLower.getP2());
        double leftJointLength = leftStraightJointLower.getP1().distance(leftStraightJointLower.getP2());
        double baseLeftJointLength = baseLeftStraightJointLower.getP1().distance(baseLeftStraightJointLower.getP2());
        Line2D baseRightSideJointLine = new Line2D.Double(cornerCutLength + baseLength / 2.0 + jointBuffer,
                                        height,
                                        cornerCutLength + baseLength / 2.0 + jointBuffer + baseRightJointLength,
                                        height);
        Line2D rightSideJointLine = new Line2D.Double(width + v12x*(cornerCutLength + jointBuffer), 
                                        height + v12y*(cornerCutLength + jointBuffer), 
                                        width + v12x*(cornerCutLength + jointBuffer + rightJointLength), 
                                        height + v12y*(cornerCutLength + jointBuffer + rightJointLength));
        Line2D leftSideJointLine = new Line2D.Double(w1 + v23x*(cornerCutLength + jointBuffer),
                                        0 + v23y*(cornerCutLength + jointBuffer),
                                        w1 + v23x*(cornerCutLength + jointBuffer + leftJointLength),
                                        0 + v23y*(cornerCutLength + jointBuffer + leftJointLength));
        Line2D baseLeftSideJointLine = new Line2D.Double(cornerCutLength + jointBuffer,
                                        height,
                                        cornerCutLength + jointBuffer + baseLeftJointLength,
                                        height);

        jointer.drawJoint(  frontContext, baseRightSideJointLine, false,
                            lastContext, baseRightStraightJointLower, true,
                            ubWoodThickness);
        jointer.drawJoint(  lastContext, baseRightStraightJointUpper, false,
                            backContext, baseRightSideJointLine, false,
                            ubWoodThickness);
        jointer.drawJoint(  frontContext, rightSideJointLine, false,
                            lastContext, rightStraightJointLower, true,
                            ubWoodThickness);
        jointer.drawJoint(  lastContext, rightStraightJointUpper, false,
                            backContext, rightSideJointLine, false,
                            ubWoodThickness);
        jointer.drawJoint(  frontContext, leftSideJointLine, false,
                            lastContext, leftStraightJointLower, true,
                            ubWoodThickness);
        jointer.drawJoint(  lastContext, leftStraightJointUpper, false,
                            backContext, leftSideJointLine, false,
                            ubWoodThickness);
        jointer.drawJoint(  frontContext, baseLeftSideJointLine, false,
                            lastContext, baseLeftStraightJointLower, true,
                            ubWoodThickness);
        jointer.drawJoint(  lastContext, baseLeftStraightJointUpper, false,
                            backContext, baseLeftSideJointLine, false,
                            ubWoodThickness);
        frontContext.drawLine((int)Math.round(baseLeftSideJointLine.getX2()), iHeight,
                              (int)Math.round(baseRightSideJointLine.getX1()), iHeight);
        backContext.drawLine((int)Math.round(baseLeftSideJointLine.getX2()), iHeight,
                              (int)Math.round(baseRightSideJointLine.getX1()), iHeight);

        //top
        lastContext.drawLine(0, 
                             0, 
                             (int)Math.round(baseRightStraightJointLower.getX1()), 
                             0);
        lastContext.drawLine((int)Math.round(baseRightStraightJointLower.getX2()), 
                             0, 
                             (int)Math.round(rightStraightJointUpper.getX1()), 
                             0);
        lastContext.drawLine((int)Math.round(rightStraightJointUpper.getX2()), 
                             0,
                             (int)Math.round(leftStraightJointUpper.getX1()), 
                             0);
        lastContext.drawLine((int)Math.round(leftStraightJointUpper.getX2()), 
                             0,
                             (int)Math.round(baseLeftStraightJointUpper.getX1()), 
                             0);
        lastContext.drawLine((int)Math.round(baseLeftStraightJointUpper.getX2()), 
                             0,
                             iLength, 
                             0);
        //bottom
        lastContext.drawLine(0, 
                             iDepth, 
                             (int)Math.round(baseRightStraightJointLower.getX1()), 
                             iDepth);
        lastContext.drawLine((int)Math.round(baseRightStraightJointLower.getX2()), 
                             iDepth, 
                             (int)Math.round(rightStraightJointUpper.getX1()), 
                             iDepth);
        lastContext.drawLine((int)Math.round(rightStraightJointLower.getX2()), 
                             iDepth,
                             (int)Math.round(leftStraightJointLower.getX1()), 
                             iDepth);
        lastContext.drawLine((int)Math.round(leftStraightJointUpper.getX2()), 
                             iDepth,
                             (int)Math.round(baseLeftStraightJointUpper.getX1()), 
                             iDepth);
        lastContext.drawLine((int)Math.round(baseLeftStraightJointUpper.getX2()), 
                             iDepth,
                             iLength, 
                             iDepth);
    }
}
