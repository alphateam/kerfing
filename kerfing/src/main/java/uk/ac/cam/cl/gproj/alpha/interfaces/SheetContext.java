/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import java.awt.*;
import java.awt.geom.Arc2D;
import java.util.ArrayList;
import java.util.List;

public class SheetContext implements DrawContext {

    /**
     * Stores a (drawing) action for replaying later with any given Graphics2D object
     */
    private interface Action {

        /**
         * Do the action with the specified Graphics2D object
         *
         * @param graphics2D The object with which to do the action
         */
        void doAction(Graphics2D graphics2D);

        /**
         * Apply the given translation to the object drawn by this action.
         *
         * @param x Amount to translate by in the x coordinate
         * @param y Amount to translate by in the y coordinate
         */
        void translate(int x, int y);

    }

    /**
     * Stores a call to drawLine
     */
    private class DrawLineAction implements Action {
        private int x1, y1, x2, y2;

        public DrawLineAction(int aX1, int aY1, int aX2, int aY2) {
            x1 = aX1;
            y1 = aY1;
            x2 = aX2;
            y2 = aY2;
        }

        public void doAction(Graphics2D g) {
            g.drawLine(x1, y1, x2, y2);
        }

        public void translate(int x, int y) {
            // Translate points
            x1 += x;
            y1 += y;
            x2 += x;
            y2 += y;
        }
    }

    /**
     * Stores a call to drawArc
     */
    private class DrawArcAction implements Action {
        private int x, y, w, h, start, extent;

        public DrawArcAction(
                int x, int y, int w, int h, int start, int extent) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.start = start;
            this.extent = extent;
        }

        public void doAction(Graphics2D g) {
            g.draw(new Arc2D.Double(x, y, w, h, start, extent, Arc2D.OPEN));
        }

        public void translate(int x, int y) {
            // Translate upper left hand corner
            this.x += x;
            this.y += y;
        }
    }

    private Color mColor;

    private List<Action> mActions;

    /**
     * Creates a new drawing context for a sheet with the specified colour
     *
     * @param aColor The colour used by this context
     */
    public SheetContext(Color aColor) {
        mColor = aColor;

        mActions = new ArrayList<Action>();
    }

    /**
     * Draws a line, using the current color, between the points (x1, y1) and (x2, y2) in this graphics context's coordinate system.
     *
     * @param x1 The first point's x coordinate.
     * @param y1 The first point's y coordinate.
     * @param x2 The second point's x coordinate.
     * @param y2 The second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        mActions.add(new DrawLineAction(x1, y1, x2, y2));
    }
    
    /**
     * Draws an arc, using the current color.
     *
     * For a description of the parameters, see {@link java.awt.Graphics2D#drawArc(int,int,int,int,int,int)}.
     */
    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        mActions.add(new DrawArcAction(x, y, width, height, startAngle, arcAngle));
    }

    /**
     * Print the contents of this context using the specified Graphics2D renderer.
     *
     * @param graphics2D The renderer to be used.
     */
    public void print(Graphics2D graphics2D) {
        graphics2D.setColor(mColor);
        for (Action a : mActions) {
            a.doAction(graphics2D);
        }
    }

    /**
     * Translates the actions in this context by (x,y)
     *
     * @param x How much to add to the x coordinate
     * @param y How much to add to the y coordinate
     */
    public void translate(int x, int y) {
        for (Action a : mActions) {
            a.translate(x,y);
        }
    }

}
