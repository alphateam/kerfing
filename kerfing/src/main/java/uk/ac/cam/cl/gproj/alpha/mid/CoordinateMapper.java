package uk.ac.cam.cl.gproj.alpha.mid;

import java.awt.*;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;

/**
 * A DrawContext that takes another DrawContext in its constructor and 
 * maps all calls to this subcontext, after remapping coordinates. 
 */
public class CoordinateMapper implements DrawContext {

    private DrawContext innerContext;
    private int[] xLoc;
    private int[] yLoc;

    /**
     * Creates a new context that maps between coordinate spaces 
     *
     * @param subContext The context to map calls to
     */
    public CoordinateMapper(DrawContext subContext, int[] xMap, int[] yMap) {
        innerContext = subContext;
        xLoc = xMap;
        yLoc = yMap;
    }

    /**
     * Draws a line in the subcontext after transforming coordinate spaces. 
     *
     * @param x1 The first point's x coordinate.
     * @param y1 The first point's y coordinate.
     * @param x2 The second point's x coordinate.
     * @param y2 The second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        int x1s = xLoc[x1];
        int y1s = yLoc[y1];
        int x2s = xLoc[x2];
        int y2s = yLoc[y2];
        innerContext.drawLine(x1s, y1s, x2s, y2s);
    }
    
    /**
     * Draws an arc in the subcontext after transforming coordinate spaces. This method is not recommended
     * as mapping between coordinate spaces is non-linear.
     *
     * For a description of the parameters, see {@link java.awt.Graphics2D#drawArc(int,int,int,int,int,int)}.
     */
    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        int xs = xLoc[x];
        int ys = yLoc[y];
        int ws = xLoc[x + width] - xLoc[x];
        int hs = yLoc[y + height] - yLoc[height];
        innerContext.drawArc(xs, ys, ws, hs, startAngle, arcAngle);
    }

}
