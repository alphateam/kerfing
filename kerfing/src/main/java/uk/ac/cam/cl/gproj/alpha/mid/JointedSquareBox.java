/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.interfaces.*;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * <p>
 *     Represents a box that is a cuboid with curved corners when looked down upon it. 
 * </p>
 */
public class JointedSquareBox extends StandardBox {

    protected static final String NAME = "Jointed Square Box";
    protected static final String DESCRIPTION = "A cuboid shaped box with curved sides. "; 
    protected static final String ICON_RESOURCE = "resources/cube.png";

    //Property definitions. Values are in mm for ease of use to the 
    //user (they are converted to 0.1mm units at start of process call)
    private final Property<Integer> mWidthProperty = new RangedProperty<Integer>("Width (mm)") {
            {
                mMaximum = 400;
                mValue = 120;
                mMinimum = 50;
            }
        };
    private final Property<Integer> mHeightProperty = new RangedProperty<Integer>("Height (mm)") {
            {
                mMaximum = 400;
                mValue = 60;
                mMinimum = 50;
            }
        };
    private final Property<Integer> mDepthProperty = new RangedProperty<Integer>("Depth (mm)") {
            {
                mMaximum = 400;
                mValue = 40;
                mMinimum = 10;
            }
        };
    private final Property<Integer> mKerfColorsProperty = new RangedProperty<Integer>("Number of colors for kerf patterns") {
            {
                mMaximum = 6;
                mValue = 3;
                mMinimum = 1;
            }
        };
    private final Property<Double> mWoodThicknessProperty = new RangedProperty<Double>("Thickness of Wood (mm)") {
            {
                mMaximum = 10.0;
                mValue = 3.0;
                mMinimum = 0.0; //let the user select 0 if they want to remove wood thickness error cancelling
            }
        };

    public String getName() {
        return NAME;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public String getIcon() {
        return ICON_RESOURCE;
    }

    public JointedSquareBox()
    {
        Properties.add(mWidthProperty);
        Properties.add(mHeightProperty);
        Properties.add(mDepthProperty);
        Properties.add(mKerfColorsProperty);
        Properties.add(mWoodThicknessProperty);
    }

    // Because writing this every time I want to cast a double to an int is so. f'in. annoying!
    private int round(double d) {
        return (int)(Math.round(d));
    }

    // Global-ish stuff
    private int width;
    private int height;
    private int depth;
    private int cornerRadius;
    private double woodThickness;
    private int straightWidth;
    private int straightHeight;
    private int cornerArcLength;
    private int sidesLength;
    private int halfJoinWidth;

    private void processProperties() {
        // Draw data values
        width = mWidthProperty.getValue() * 10;
        height = mHeightProperty.getValue() * 10;
        depth = mDepthProperty.getValue() * 10;
        cornerRadius = 200;
        woodThickness = mWoodThicknessProperty.getValue() * 10;

        // Computed values
        straightWidth = width - cornerRadius - cornerRadius;
        straightHeight = height - cornerRadius - cornerRadius;
        cornerArcLength = (int)(0.5*Math.PI*((double)cornerRadius + 0.5*woodThickness));
        sidesLength = 2*(straightWidth) + 2*(straightHeight) + 4*cornerArcLength;
        halfJoinWidth = straightWidth/2 - round(woodThickness/2);
    }

    private void drawTopBottom(Sheet top, Sheet bottom) {
        top.setBoundingShape(new Rectangle(0, 0, width, height));
        bottom.setBoundingShape(new Rectangle(0, 0, width, height));

        SheetContext[] lidContexts = new SheetContext[] { top.getNewContext(), bottom.getNewContext() };
        for (SheetContext context : lidContexts) {
            context.drawArc(0, 0, cornerRadius * 2, cornerRadius * 2, 90, 90);
            context.drawLine(cornerRadius, 0, cornerRadius, round(woodThickness));
            context.drawLine(width - cornerRadius, 0, width - cornerRadius, round(woodThickness));

            context.drawLine(cornerRadius + halfJoinWidth, 0, cornerRadius + halfJoinWidth, round(woodThickness));
            context.drawLine(cornerRadius + halfJoinWidth, 0, cornerRadius + halfJoinWidth + round(woodThickness), 0);
            context.drawLine(cornerRadius + halfJoinWidth + round(woodThickness), 0, cornerRadius + halfJoinWidth + round(woodThickness), round(woodThickness));

            context.drawArc(straightWidth, 0, cornerRadius * 2, cornerRadius * 2, 0, 90);
            context.drawLine(width, cornerRadius, width - round(woodThickness), cornerRadius);
            context.drawLine(width, height - cornerRadius, width - round(woodThickness), height - cornerRadius);

            context.drawArc(straightWidth, straightHeight, cornerRadius * 2, cornerRadius * 2, 270, 90);
            context.drawLine(width - cornerRadius, height, width - cornerRadius, height - round(woodThickness));
            context.drawLine(cornerRadius, height, cornerRadius, height - round(woodThickness));

            context.drawArc(0, straightHeight, cornerRadius * 2, cornerRadius * 2, 180, 90);
            context.drawLine(0, height - cornerRadius, round(woodThickness), height - cornerRadius);
            context.drawLine(0, cornerRadius, round(woodThickness), cornerRadius);
        }
    }

    private void drawKerfs(Sheet sheet) {
        KerfGenerator kerfer = getKerfGenerator();

        ArrayList<SheetContext> kerfContexts = new ArrayList<SheetContext>();
        int kerfColors = mKerfColorsProperty.getValue();
        for (int i = 0; i < kerfColors; ++i) {
            kerfContexts.add(sheet.getNewContext());
        }

        double loc = straightWidth / 2;

        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightHeight;

        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightWidth;

        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightHeight;

        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
    }

    private void drawJoints(Sheet sides, Sheet top, Sheet bottom) {
        JointGenerator joiner = new FingerJointGenerator();

        SheetContext sidesJoinContext = sides.getNewContext();
        SheetContext[] lidContexts = new SheetContext[] { top.getNewContext(), bottom.getNewContext() };

        // Join the sides going around
        joiner.drawJoint(sidesJoinContext, new Line2D.Double(woodThickness/2, 0, woodThickness/2, depth), false,
                sidesJoinContext, new Line2D.Double(sidesLength - woodThickness/2, 0, sidesLength - woodThickness/2, depth), true,
                round(woodThickness));

        // Join the top to the sides
        for (int side = 0; side < 2; ++side) {
            int loc = round(woodThickness/2);

            joiner.drawJoint(sidesJoinContext, new Line2D.Double(loc, side * depth, loc + halfJoinWidth, side * depth), side == 0,
                    lidContexts[side], new Line2D.Double(cornerRadius, woodThickness, cornerRadius + halfJoinWidth, woodThickness), true,
                    round(woodThickness));
            loc += straightWidth / 2 - round(woodThickness/2);

            sidesJoinContext.drawLine(loc, side * depth, loc + cornerArcLength, side * depth);
            loc += cornerArcLength;

            joiner.drawJoint(sidesJoinContext, new Line2D.Double(loc, side * depth, loc + straightHeight, side * depth), side == 0,
                    lidContexts[side], new Line2D.Double(width - woodThickness, cornerRadius, width - woodThickness, cornerRadius + straightHeight), true,
                    round(woodThickness));
            loc += straightHeight;

            sidesJoinContext.drawLine(loc, side * depth, loc + cornerArcLength, side * depth);
            loc += cornerArcLength;

            joiner.drawJoint(sidesJoinContext, new Line2D.Double(loc, side * depth, loc + straightWidth, side * depth), side == 0,
                    lidContexts[side], new Line2D.Double(cornerRadius + straightWidth, height - woodThickness, cornerRadius, height - woodThickness), true,
                    round(woodThickness));
            loc += straightWidth;

            sidesJoinContext.drawLine(loc, side * depth, loc + cornerArcLength, side * depth);
            loc += cornerArcLength;

            joiner.drawJoint(sidesJoinContext, new Line2D.Double(loc, side * depth, loc + straightHeight, side * depth), side == 0,
                    lidContexts[side], new Line2D.Double(woodThickness, cornerRadius + straightHeight, woodThickness, straightHeight), true,
                    round(woodThickness));
            loc += straightHeight;

            sidesJoinContext.drawLine(loc, side * depth, loc + cornerArcLength, side * depth);
            loc += cornerArcLength;

            joiner.drawJoint(sidesJoinContext, new Line2D.Double(loc, side * depth, loc + halfJoinWidth, side * depth), side == 0,
                    lidContexts[side], new Line2D.Double(cornerRadius + straightWidth - halfJoinWidth, woodThickness, cornerRadius + straightWidth, woodThickness), true,
                    round(woodThickness));
            loc += straightWidth / 2 - round(woodThickness/2);
        }
    }

    public void process(GraphicsPrinter gp) {
        processProperties();

        if (gp == null) {
            System.err.println("Square Box could not find a graphics printer. ");
            return;
        }

        // Draw top and bottom of box
        Sheet top = gp.getNewSheet();
        Sheet bottom = gp.getNewSheet();
        drawTopBottom(top, bottom);

        // Draw sides of box
        Sheet sides = gp.getNewSheet();
        sides.setBoundingShape(new Rectangle(-round(woodThickness / 2), -round(woodThickness),
                sidesLength + round(woodThickness / 2), depth + round(woodThickness)));
        // Draw kerfs on the sides of the box
        drawKerfs(sides);

        // Draw joints on everything
        drawJoints(sides, top, bottom);
    }

}
