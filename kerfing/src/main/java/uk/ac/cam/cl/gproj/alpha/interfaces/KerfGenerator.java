/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import java.util.List;

/**
 * <p>
 *     Represents an object that can draw kerf patterns on a surface. 
 * </p>
 */
public interface KerfGenerator {

    /**
     * Gives a name to the generator.
     *
     * @return A name for the generator.
     */
    public String getName();

    /**
     * Gives a more detailed description of the kerfs.
     *
     * @return A description for the generator.
     */
    public String getDescription();

    /**
     * Gives an icon for the generator.
     *
     * @return A string reference to an icon to represent the generator.
     */
    public String getIcon();

    /**
     * Gets a decription for how pliable the kerf is. 
     *
     * @return A string description of pliability.
     */
    public String getPliability();

    /**
     * Gets a decription for how much tensile strength the kerf has.  
     *
     * @return A string description of strength.
     */
    public String getTensileStrength();

    /**
     * Gets a decription for how much torsonal strength the kerf has. 
     *
     * @return A string description of strength.
     */
    public String getTorsonalStrength();

    /**
     * Gets a decription for how much normal strength the kerf has. 
     *
     * @return A string description of strength.
     */
    public String getNormalStrength();

    /**
     * Draws the cuts in the given rectangle to kerf by the given power. power is expressed as if the 
     * the output kerf is molded to a lens. 
     *
     * @param contexts The SheetContexts to draw to. 
     * @param x1 X Coordinate of start of rectangle. 
     * @param y1 Y Coordinate of start of rectangle. 
     * @param x2 X Coordinate of end of rectangle. 
     * @param y2 Y Coordinate of end of rectangle. 
     * @param vertical True if kerfing around in vertical direction. False if kerfing around horizontal direction. 
     * @param curvePower The amount of curvature to the kerf. curvePower = 1 / distance to focus
     */
    public void drawKerfs(List<SheetContext> contexts, double x1, double y1, double x2, double y2, boolean vertical, double curvePower);

}
