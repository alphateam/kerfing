package uk.ac.cam.cl.gproj.alpha.interfaces;

import uk.ac.cam.cl.gproj.alpha.mid.JointedSquareBox;
import uk.ac.cam.cl.gproj.alpha.mid.SquareBox;
import uk.ac.cam.cl.gproj.alpha.mid.TriangleBox;

/**
 * <p>
 *     Helper class for Box-related functionality
 * </p>
 */
public final class BoxHelper {

    private BoxHelper() { }

    /**
     * Gets an array containing a new instance of each known box implementation. 
     *
     * @return An array of known boxes. 
     */
    public static Box[] getBoxes() {
        return new Box[] {
            new SquareBox(), new TriangleBox(), new JointedSquareBox()
        };
    }
}
