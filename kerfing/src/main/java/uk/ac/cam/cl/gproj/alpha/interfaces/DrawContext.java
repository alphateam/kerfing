package uk.ac.cam.cl.gproj.alpha.interfaces;

/**
 * <p>
 *     An interface used to represent the part of the contract SheetContext exposes that is needed by Box 
 *     implementations to draw. This exists so decorator-pattern style designs can be efficiently used in 
 *     box processing without the redundant overhead in the SheetContext class. 
 * </p>
 */
public interface DrawContext {

    /**
     * Draws a line, using the current color, between the points (x1, y1) and (x2, y2) in this graphics context's coordinate system.
     *
     * @param x1 The first point's x coordinate.
     * @param y1 The first point's y coordinate.
     * @param x2 The second point's x coordinate.
     * @param y2 The second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2);
    
    /**
     * Draws an arc, using the current color.
     *
     * For a description of the parameters, see {@link java.awt.Graphics2D#drawArc(int,int,int,int,int,int)}.
     */
    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle);

}
