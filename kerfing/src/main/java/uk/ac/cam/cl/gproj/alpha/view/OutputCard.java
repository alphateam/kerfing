/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import uk.ac.cam.cl.gproj.alpha.exceptions.LayoutException;
import uk.ac.cam.cl.gproj.alpha.interfaces.Box;
import uk.ac.cam.cl.gproj.alpha.interfaces.GraphicsPrinter;
import uk.ac.cam.cl.gproj.alpha.output.ExecutableGraphics2D;
import uk.ac.cam.cl.gproj.alpha.output.OutputSVGGraphics2D;
import uk.ac.cam.cl.gproj.alpha.output.DXFGraphics2D;


public class OutputCard extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	public static final String[] fileTypes = {
		"SVG",    
		"DXF",

	};

	private final WizardCard parent;
	private Box box;
	private static final JLabel selectFileLabel = new JLabel("Select output file type");
	private static final JLabel selectdirectoryLabel = new JLabel("Select output directory");
	private static final JLabel setNameLabel = new JLabel("Set output name");

	private JPanel infoPanel = new JPanel();
	private JPanel navigationPanel = new JPanel();

	private JButton saveButton = new JButton("Save");
	private JButton finishButton = new JButton("Finish project");
	private final JButton backButton = new JButton("Back");

	private final JTextField widthField = new JTextField("9000");
	private final JTextField heightField = new JTextField("6000");

	private JTextField  destinationFileName = new JTextField();
	private JComboBox typeCombo = new JComboBox();
	private JFileChooser chooser = new JFileChooser();
	private JTextField destinationDirectoryField = new JTextField(20);
	private GraphicsPrinter gp= new GraphicsPrinter();

	public OutputCard(WizardCard wc) {
		this.parent = wc;
		setLayout(new BorderLayout());

		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder("Output file attributes"));

		infoPanel.add(new JLabel("Width (0.1 mm)"), setGridBagConstriants(0,0,1,1));
		infoPanel.add(widthField, setGridBagConstriants(1,0,2,1));
		infoPanel.add(new JLabel("Height (0.1 mm)"), setGridBagConstriants(0,1,1,1));
		infoPanel.add(heightField, setGridBagConstriants(1,1,2,1));
		infoPanel.add(selectFileLabel, setGridBagConstriants(0,2,1,1));
		typeCombo = new JComboBox(fileTypes);
		infoPanel.add(typeCombo, setGridBagConstriants(1,2,1,1));
		
		infoPanel.add(selectdirectoryLabel, setGridBagConstriants(0,3,1,1));
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogTitle("Choose Output Directory");
		saveButton = new JButton("Save in...");
		infoPanel.add(saveButton, setGridBagConstriants(1,3,1,1));
		infoPanel.add(destinationDirectoryField, setGridBagConstriants(2,3,1,1));
        infoPanel.add(setNameLabel, setGridBagConstriants(0,4,1,1));
        infoPanel.add(destinationFileName, setGridBagConstriants(1,4,2,1));
		
		navigationPanel.setLayout(new BorderLayout());
		navigationPanel.add(backButton, BorderLayout.WEST);
		navigationPanel.add(finishButton, BorderLayout.EAST);
		
		add(infoPanel, BorderLayout.CENTER);
		add(navigationPanel, BorderLayout.SOUTH);
		
		//ActionListeners
		saveButton.addActionListener(this);
        backButton.addActionListener(this);
        finishButton.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
	    int width = Integer.parseInt(widthField.getText());
	    int height = Integer.parseInt(heightField.getText());
	    
	    
		//Handle open button action.

		if (e.getSource() == saveButton) {
			if (chooser.showOpenDialog(new JFrame()) == JFileChooser.APPROVE_OPTION) {
			    destinationDirectoryField.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		} else if (e.getSource() == finishButton) {
			OutputStream os = null;
			try {
				os = new FileOutputStream(destinationDirectoryField.getText()+destinationFileName.getText()+"."+ typeCombo.getSelectedItem().toString());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			box.process(gp);
			try {
				gp.layOut(width, height);
			} catch (LayoutException e1) {
				e1.printStackTrace();
			}
			Graphics2D graphics = null ;
			if(typeCombo.getSelectedItem().toString().equals("SVG")){
				// This line may be the source of error: Says, cannot convert from type OutputSVGGraphics2D to Graphics2D
			    // If throws an error for you, comment out to see the full GUI working.
			    graphics = new OutputSVGGraphics2D (width, height);
			}else if (typeCombo.getSelectedItem().toString().equals("DXF")){
			    graphics = new DXFGraphics2D();
			}
			gp.print(graphics);
			try {
				((ExecutableGraphics2D)graphics).finalizeWrite(os);
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		} else if (e.getSource() == backButton){
            ((CardLayout) parent.getLayout()).previous(this.getParent());
        } 

	}

	public void setBox(Box box) {
		this.box = box;
	}
	
	public GridBagConstraints setGridBagConstriants(int gridx, int gridy, int gridwidth, int gridheight){
        return new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 0, 0, GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0);
    }
}


