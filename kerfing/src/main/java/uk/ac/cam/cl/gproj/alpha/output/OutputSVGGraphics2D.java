/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.dom.GenericDOMImplementation;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.DOMImplementation;

import java.awt.Dimension;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;

import java.awt.BasicStroke;

/**
 * This class act as a wrapper around Apache Batik's SVGGraphics2D,
 * implementing ExecutableGraphics2D.
 */
public class OutputSVGGraphics2D extends SVGGraphics2D implements ExecutableGraphics2D {
  
  /** 
   * Standard options from SVG 
   *
   * @param width width of Canvas (assumed equal to size of material sheet)
   * @param height height of Canvas  (assumed equal to size of material sheet)
  **/

  private int width, height;

  public OutputSVGGraphics2D(int width, int height) {
    super(
        GenericDOMImplementation.getDOMImplementation().
        createDocument("http://www.w3.org/2000/svg", "svg", null)
    );

    this.width = width;
    this.height = height;

    this.setStroke(new BasicStroke(0.0001f));
  }
  
  /**
   * Finalizes write by writing the SVG to the given OutputStream
   *
  **/
  public void finalizeWrite(OutputStream output) throws IOException {
    OutputStreamWriter out = new OutputStreamWriter(output, "UTF-8");
    boolean useCss = true;

    Element svgRoot = this.getRoot();
    svgRoot.setAttributeNS(null, "width", width/10.0 + "mm");
    svgRoot.setAttributeNS(null, "height", height/10.0 + "mm");
    svgRoot.setAttributeNS(null, "viewBox", "0 0 " + width + " " + height);
    this.stream(svgRoot, out, useCss);
    out.close();
  }
} 
