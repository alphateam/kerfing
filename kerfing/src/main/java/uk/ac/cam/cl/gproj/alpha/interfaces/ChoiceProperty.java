/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;

import java.util.List;
import java.util.TreeSet;

/**
 * <p>
 *     A property represented by a choice between some options.
 *     Should generally be handled using addChoice and removeChoice.
 * </p>
 *
 * <p>
 *     Initialises the value, so will be shown by default.
 *     The value is a TreeSet&lt;Integer&gt; containing the indexes of the selected choices from the choice list.
 * </p>
 *
 * <p>
 *     By default, there can only be a single choice. Use setMultipleChoice(true) to switch to multiple choice.
 * </p>
 *
 * @param <T> Type of objects to choose from
 */
public abstract class ChoiceProperty<T> extends Property<TreeSet<Integer>> {

    protected boolean mAllowMultipleChoice = false; // Single choice by default

    /**
     * Constructs a new property with the specified name
     *
     * @param name The property's name
     */
    public ChoiceProperty(String name) {
        super(name);

        resetChoice();
    }

    /**
     * Sets the choices
     *
     * @param aChoices A new set of choices, as defined above
     * @throws InvalidArgumentException If aChoices contains indexes that are out of the bounds of the choice list or
     * has more than 1 element when multiple choices are not allowed
     */
    @Override
    public void setValue(TreeSet<Integer> aChoices) throws InvalidArgumentException {
        if (aChoices == null) {
            throw new InvalidArgumentException("Null not allowed");
        }
        if (!aChoices.isEmpty() && (aChoices.first() < 0 || aChoices.last() >= getChoiceList().size())) {
            throw new InvalidArgumentException("Choice out of bounds");
        }
        if (!isMultipleChoice() && aChoices.size() > 1) {
            throw new InvalidArgumentException("Too many choices");
        }

        mValue = aChoices;
    }

    /**
     * Convenience setter for a single choice. Note that this will clear whatever choices are currently stored.
     *
     * @param aChoice The new choice, an index in the choice list
     * @throws InvalidArgumentException If aChoice is out of the bounds of the choice list
     */
    public void setValue(Integer aChoice) throws InvalidArgumentException {
        if (aChoice < 0 || aChoice >= getChoiceList().size()) {
            throw new InvalidArgumentException("Choice out of bounds");
        }

        resetChoice();
        addChoice(aChoice);
    }

    /**
     * Remove all choices
     */
    public void resetChoice() {
        mValue = new TreeSet<Integer>();
    }

    /**
     * Add a new choice to the set of active choices
     *
     * @param aChoice The index of the new choice in the choice list
     * @throws InvalidArgumentException If aChoice is out of the bounds of the choice list or if there is already 1
     * choice active and multiple choices are not allowed
     */
    public void addChoice(Integer aChoice) throws InvalidArgumentException {
        if (aChoice < 0 || aChoice >= getChoiceList().size()) {
            throw new InvalidArgumentException("Choice out of bounds");
        }
        if (!isMultipleChoice() && !getValue().isEmpty()) {
            throw new InvalidArgumentException("Cannot add another choice");
        }

        mValue.add(aChoice);
    }

    /**
     * Remove a choice from the set of active choices. The choice does not necessarily have to be in the set in the
     * first place.
     *
     * @param aChoiceIndex The index of the new choice in the choice list
     */
    public void removeChoice(Integer aChoiceIndex) {
        mValue.remove(aChoiceIndex);
    }

    /**
     * Checks if multiple choices are allowed
     *
     * @return Yes or no
     */
    public boolean isMultipleChoice() {
        return mAllowMultipleChoice;
    }

    /**
     * Sets if multiple choices are allowed
     *
     * @param aMultipleChoice Yes or no
     */
    public void setMultipleChoice(boolean aMultipleChoice) {
        mAllowMultipleChoice = aMultipleChoice;
    }

    /**
     * Get a list of the possible choices
     *
     * @return A list of the possible choices
     */
    public abstract List<T> getChoiceList();

}
