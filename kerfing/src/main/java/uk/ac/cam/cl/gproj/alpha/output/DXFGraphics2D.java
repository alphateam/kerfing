/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.output;

import uk.ac.cam.cl.gproj.alpha.exceptions.NoACIException;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.text.AttributedCharacterIterator;

import org.apache.batik.ext.awt.g2d.AbstractGraphics2D;
import org.apache.batik.ext.awt.g2d.GraphicContext;

/**
 * This implementation of the java.awt.Graphics2D abstract class allows
 * users to generate AutoCAD DXF (Drawing Interchange Format) content
 * from Java code.
 *
 * DXFGraphics2D generates a DXF document that can be written to an
 * output stream using the finalizeWrite method.
 *
 * The various graphic context attributes (e.g., AffineTransform,
 * Paint) are managed by a GraphicContext object.
 *
 * Currently, only arcs and lines as well as the colour attribute are
 * supported. All attempts to draw a shape that is not an arc or a line
 * will result in an error message written to System.err, but no
 * exception will be thrown.
 *
 * The source code is based on Apache Batik's excellent
 * SVGraphics2D and DefaultGraphics2D classes.
 */
public class DXFGraphics2D
        extends AbstractGraphics2D
        implements ExecutableGraphics2D {

    /* The DXF document to write to */
    private DXFDocument dxfDoc;

    /**
     * Used to create proper font metrics
     */
    private Graphics2D fmg;

    /**
     * Default constructor
     */
    public DXFGraphics2D() {
        super(false); // set textAsShapes to false
        this.gc = new GraphicContext();

        /* Initialise private fields */
        dxfDoc = new DXFDocument();

        BufferedImage bi
            = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);

        fmg = bi.createGraphics();
    }

    /**
     * This constructor supports the create method
     */
    public DXFGraphics2D(DXFGraphics2D g){
        super(g);
    }

    /**
     * Creates a new <code>Graphics</code> object that is
     * a copy of this <code>Graphics</code> object.
     * @return     a new graphics context that is a copy of
     *             this graphics context.
     */
    public Graphics create(){
        return new DXFGraphics2D(this);
    }

    /**
     * Write the buffered DXF data to the given output stream.
     * @param      out the output stream to write to.
     */
    public void finalizeWrite(OutputStream out) throws IOException {
        dxfDoc.writeTo(out);
    }

    /**
     * Print an error message to inform the user about an unsupported
     * Graphics2D method.
     * @param     methodName  name of the unsupported method.
     */
    private void reportUnsupportedMethod(String methodName) {
        System.err.println("DXFGraphics2D: " + methodName + " not supported.");
    }

    /**
     * Print an error message to inform the user about a currently
     * unsupported Graphics2D method.
     * @param     methodName  name of the currently unsupported method.
     */
    private void reportCurrentlyUnsupportedMethod(String methodName) {
        System.err.println("DXFGraphics2D: " + methodName + " currently not supported.");
    }

    /**
     * Disposes of this graphics context and releases
     * any system resources that it is using.
     * A <code>Graphics</code> object cannot be used after
     * <code>dispose</code>has been called.
     * <p>
     * When a Java program runs, a large number of <code>Graphics</code>
     * objects can be created within a short time frame.
     * Although the finalization process of the garbage collector
     * also disposes of the same system resources, it is preferable
     * to manually free the associated resources by calling this
     * method rather than to rely on a finalization process which
     * may not run to completion for a long period of time.
     * <p>
     * Graphics objects which are provided as arguments to the
     * <code>paint</code> and <code>update</code> methods
     * of components are automatically released by the system when
     * those methods return. For efficiency, programmers should
     * call <code>dispose</code> when finished using
     * a <code>Graphics</code> object only if it was created
     * directly from a component or another <code>Graphics</code> object.
     * @see         java.awt.Graphics#finalize
     * @see         java.awt.Component#paint
     * @see         java.awt.Component#update
     * @see         java.awt.Component#getGraphics
     * @see         java.awt.Graphics#create()
     */
    public void dispose(){
        // Nothing to do.
    }

    /**
     * Strokes the outline of a <code>Shape</code> using the settings of the
     * current <code>Graphics2D</code> context.  The rendering attributes
     * applied include the <code>Clip</code>, <code>Transform</code>,
     * <code>Paint</code>, <code>Composite</code> and
     * <code>Stroke</code> attributes.
     * @param s the <code>Shape</code> to be rendered
     * @see #setStroke(java.awt.Stroke)
     * @see #setPaint(java.awt.Paint)
     * @see java.awt.Graphics#setColor
     * @see #setTransform(AffineTransform)
     * @see #setClip(Shape)
     * @see #setComposite(java.awt.Composite)
     */
    public void draw(Shape s) {
        // Only BasicStroke is supported.
        if (!(gc.getStroke() instanceof BasicStroke)) {
            // FIXME: Deal with it. Throw and exception? Which?
            return;
        }

        DXFShape dxfShape = new DXFShape(s, gc);
        dxfDoc.addEntity(dxfShape);
    }

    /**
     * Renders the text specified by the specified <code>String</code>,
     * using the current <code>Font</code> and <code>Paint</code> attributes
     * in the <code>Graphics2D</code> context.
     * The baseline of the first character is at position
     * (<i>x</i>,&nbsp;<i>y</i>) in the User Space.
     * The rendering attributes applied include the <code>Clip</code>,
     * <code>Transform</code>, <code>Paint</code>, <code>Font</code> and
     * <code>Composite</code> attributes. For characters in script systems
     * such as Hebrew and Arabic, the glyphs can be rendered from right to
     * left, in which case the coordinate supplied is the location of the
     * leftmost character on the baseline.
     * @param s the <code>String</code> to be rendered
     * @param x the x coordinate where the <code>String</code> should be
     *          rendered
     * @param y the y coordinate where the <code>String</code> should be
     *          rendered
     * @see #setPaint(java.awt.Paint)
     * @see java.awt.Graphics#setColor
     * @see java.awt.Graphics#setFont
     * @see #setTransform(AffineTransform)
     * @see #setComposite(java.awt.Composite)
     * @see #setClip(Shape)
     */
    public void drawString(String s, float x, float y){
        reportCurrentlyUnsupportedMethod("drawString(String)");
    }

    /**
     * Renders the text of the specified iterator, using the
     * <code>Graphics2D</code> context's current <code>Paint</code>. The
     * iterator must specify a font
     * for each character. The baseline of the
     * first character is at position (<i>x</i>,&nbsp;<i>y</i>) in the
     * User Space.
     * The rendering attributes applied include the <code>Clip</code>,
     * <code>Transform</code>, <code>Paint</code>, and
     * <code>Composite</code> attributes.
     * For characters in script systems such as Hebrew and Arabic,
     * the glyphs can be rendered from right to left, in which case the
     * coordinate supplied is the location of the leftmost character
     * on the baseline.
     * @param iterator the iterator whose text is to be rendered
     * @param x the x coordinate where the iterator's text is to be rendered
     * @param y the y coordinate where the iterator's text is to be rendered
     * @see #setPaint(java.awt.Paint)
     * @see java.awt.Graphics#setColor
     * @see #setTransform(AffineTransform)
     * @see #setComposite(java.awt.Composite)
     * @see #setClip(Shape)
     */
     public void drawString(AttributedCharacterIterator iterator,
                            float x, float y) {
         reportCurrentlyUnsupportedMethod("drawString(AttributedCharacterIterator)");
     }
    
    /**
     * Returns the device configuration associated with this
     * <code>Graphics2D</code>.
     */
    public GraphicsConfiguration getDeviceConfiguration(){
        reportCurrentlyUnsupportedMethod("getDeviceConviguration");
        return null;
    }

    /**
     * Gets the font metrics for the specified font.
     * @return    the font metrics for the specified font.
     * @param     f the specified font
     * @see       java.awt.Graphics#getFont
     * @see       java.awt.FontMetrics
     * @see       java.awt.Graphics#getFontMetrics()
     */
    public FontMetrics getFontMetrics(Font f){
        return fmg.getFontMetrics(f);
    }

    public void setXORMode(Color c1){
        reportUnsupportedMethod("setXORMode");
    }

    public void copyArea(int x, int y, int width, int height,
                         int dx, int dy){
        reportUnsupportedMethod("copyArea");
    }

    public boolean drawImage(Image img, int x, int y, ImageObserver observer){
        reportUnsupportedMethod("drawImage");
        return true;
    }

    public boolean drawImage(Image img, int x, int y,
                             int width, int height,
                             ImageObserver observer) {
        reportUnsupportedMethod("drawImage");
        return true;
    }

    public void drawRenderedImage(RenderedImage img,
                                  AffineTransform xform) {
        reportUnsupportedMethod("drawRenderedImage");
    }

     public void drawRenderableImage(RenderableImage img,
                                     AffineTransform xform){
        reportUnsupportedMethod("drawRenderableImage");
     }

    public void fill(Shape s){
        reportUnsupportedMethod("fill");
    }

}
