/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;


import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Standard-Implementation of Sheet
 * The colors here are in order of the CL's laser cutter
 * Might have to adjust this (re-implement color ordering)
 * for the laser cutter at the makespace site
 */
public class StandardSheet extends Sheet {

    private int colorCount;
    private List<Color> colorList;
    
    /**
     * Initialises a new StandardSheet. Needs to be public
     * since accessible by front-end
     */
    public StandardSheet() {
      super();
      fillColorList();
    }
    
    /**
     * initialises the color-list
     * with the first 9 colors of the CL's laser cutter
     * Will have to adopt for makespace laser cutter
     * 
     * TODO: use RGB definitions from laser cutter preferences instead of pre-defined Colors
     */
    private void fillColorList() {
      colorList = new ArrayList<Color>();
      colorList.add(Color.black);
      colorList.add(Color.red);
      colorList.add(Color.green);
      colorList.add(Color.yellow);
      colorList.add(Color.blue);
      colorList.add(Color.magenta);
      colorList.add(Color.cyan);
      colorList.add(Color.orange);
      colorList.add(Color.pink);
      
      colorCount = 0;
    }

    /**
     * Returns a new colour for a new graphics context. 
     * If the maximum number of colors is exhausted, then the first color is returned again
     * otherwise a new color is returned, that is drawn after everything previously added
     *  
     * @return A new colour
     */
    protected Color getNewColor() {
      Color newColor = colorList.get(colorCount);
      if (colorCount == colorList.size() - 1) 
        colorCount = 0;
      else 
        colorCount++;
      return newColor;
    }

}
