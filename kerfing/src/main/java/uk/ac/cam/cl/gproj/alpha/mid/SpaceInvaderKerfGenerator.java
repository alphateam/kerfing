package uk.ac.cam.cl.gproj.alpha.mid;

import java.util.List;
import java.util.ArrayList;
import java.lang.ArithmeticException;
import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;


/**
 * A kerf line generator for those with odd interests. 
 * 
 * <p>
 *     This draws space invaders on the wood. 
 * </p>
 */
public class SpaceInvaderKerfGenerator implements KerfGenerator {

    private static final String Name = "Space Invader Kerf Generator";
    private static final String Description = "Generates a pattern of space invader style shapes. ";
    private static final String Icon = "resources/kerf-alieninvader.jpg";
    private static final String PLIABILITY = "Fair";
    private static final String TENSILE_STRENGTH = "Good";
    private static final String TORSONAL_STRENGTH = "Poor";
    private static final String NORMAL_STRENGTH = "Poor";

    //A constant that scales how big the invaders would be (before rounding)
    private static final double BASE_INVADER_WIDTH = 1.0;
    //A constant of how high each invader is, in proportion to its width
    private static final double HEIGHT_TO_WIDTH_RATIO = 7.0 / 11.0;
    //fraction of alien that touces it's horizontal neighbour
    private static final double THICKNESS_RATIO = 0.15; 

    /**
     * Default Class Constructor.  
     */
    public SpaceInvaderKerfGenerator() { }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getIcon() {
        return Icon;
    }

    public String getPliability() {
        return PLIABILITY;
    }

    public String getTensileStrength() {
        return TENSILE_STRENGTH;
    }

    public String getTorsonalStrength() {
        return TORSONAL_STRENGTH;
    }

    public String getNormalStrength() {
        return NORMAL_STRENGTH;
    }

    public void drawKerfs(List<SheetContext> contexts, double x1, double y1, double x2, double y2, boolean vertical, double curvePower) {
        if (vertical) {
            //remaps coordinates and uses horizontal drawing so the output gets mapped to vertical cuts
            List<CoordinateFlipper> newContexts = new ArrayList<CoordinateFlipper>();
            for (SheetContext s : contexts) {
                newContexts.add(new CoordinateFlipper(s));
            }
            drawKerfsHorizontal(newContexts, y1, x1, y2, x2, curvePower);
        } else {
            drawKerfsHorizontal(contexts, x1, y1, x2, y2, curvePower);
        }
    }

    private void drawKerfsHorizontal(List<? extends DrawContext> contexts, double x1, double y1, double x2, double y2, double curvePower) {     
        if (x2 < x1) {
            double tmp = x2;
            x2 = x1;
            x1 = tmp;
        }
        if (y2 < y1) {
            double tmp = y2;
            y2 = y1;
            y1 = tmp;
        }
        double width = x2 - x1;
        double height = y2 - y1;
        //An estimation for scaling invaders with varying curves and patch sizes
        double idealInvaderWidth = BASE_INVADER_WIDTH / curvePower;
        double idealInvaderHeight = HEIGHT_TO_WIDTH_RATIO * idealInvaderWidth;
        double idealInvaderCountWidth = width / idealInvaderWidth;
        double idealInvaderCountHeight = height / idealInvaderHeight;
        int actualInvaderCountWidth = (int)Math.round(idealInvaderCountWidth); //assume these casts always work fine (they don't)
        int actualInvaderCountHeight = (int)Math.round(idealInvaderCountHeight); //assume rounding is insignificant (it isn't)
        //if any of the actual counts are 0, there's not enough curve to justify any invaders
        //in this case, the actual dimensions will go to NaN, but that's okay because we'll then skip drawing
        double actualInvaderWidth = width / ((double)actualInvaderCountWidth);
        double actualInvaderHeight = height / ((double)actualInvaderCountHeight);
        double thicknessRatio = THICKNESS_RATIO; //possibly optimise this value here depending on area size / curve power / real world data. 
        for (int i = 0; i < actualInvaderCountWidth; ++i) {
            for (int j = 0; j < actualInvaderCountHeight; ++j) {
                drawAlien(contexts, x1 + i * actualInvaderWidth, y1 + j * actualInvaderHeight, actualInvaderWidth, 
                    actualInvaderHeight, thicknessRatio, i == 0, i == actualInvaderCountWidth - 1,
                    j == 0, j == actualInvaderCountHeight - 1);
            }
        }
    }

    /**
     * Draws an individual space invader. 
     *
     * @param contexts A list of all contexts that can be drawn to. 
     * @param x The x coordinate of the top left corner of drawing. 
     * @param y The y coordinate of the top left corner of drawing. 
     * @param width The width of the block to draw to. 
     * @param height The height of the block to draw to. 
     * @param thicknessRatio The fraction of wood on side edge that connects to neighbouring invader. 
     * @param firstX A If it is the first invader in the x dimension in a set of invaders. 
     * @param lastX If it is the last invader in the x dimension in a set of invaders. 
     * @param firstY If it is the first invader in the y dimension in a set of invaders. 
     * @param lastY If it is the last invader in the y dimension in a set of invaders. 
     */
    private void drawAlien(List<? extends DrawContext> contexts, double x, double y, double width, double height, 
        double thicknessRatio, boolean firstX, boolean lastX, boolean firstY, boolean lastY) {

        // 11x7 pixel space invader. Reference: 
        // □□□■□□□■□□□
        // □□■■■■■■■□□
        // □■■□■■■□■■□
        // ■■■■■■■■■■■
        // ■□■■■■■■■□■
        // □□■□□□□□■□□
        // □□■■■□■■■□□

        // 7 pixels high -> 4th row connects horizontally and has custom height
        double normalRowHeight = height * (1 - thicknessRatio) / 6;
        double connectingRowHeight = height * thicknessRatio;

        // 11 pixels wide -> all equal width
        double columnWidth = width / 11;

        //create arrays of cell locations
        //these represent mappings from coordinates in the space invaders frame to the actual output area
        int[] locx = new int[12];
        int[] locy = new int[8];
        for (int i = 0; i < 12; ++i) {
            double xloc = x + columnWidth * i;
            locx[i] = (int)xloc;
        }
        for (int j = 0; j < 8; ++j) {
            double yloc = y + ((j < 4) ? (normalRowHeight * j) : connectingRowHeight + (normalRowHeight * (j - 1)));
            locy[j] = (int)yloc;
        }
        
        //Creates a context to remap from Space Invader coordinate space to output coordinate space
        //Also maps calls to a randomly chosen context (this is nondeterministic)
        DrawContext g = new CoordinateMapper(new RandomPickSheetContext(contexts), locx, locy);

        //cut upper left
        if (firstX) {
            g.drawLine(0, 0, 0, 3);
        }
        g.drawLine(0, 3, 1, 3);
        g.drawLine(1, 3, 1, 2);
        g.drawLine(1, 2, 2, 2);
        g.drawLine(2, 2, 2, 1);
        g.drawLine(2, 1, 3, 1);
        g.drawLine(3, 1, 3, 0);
        g.drawLine(3, 0, 2, 0);
        if (firstY) {
            g.drawLine(2, 0, 0, 0);
        }

        //cut upper
        g.drawLine(4, 0, 4, 1);
        g.drawLine(4, 1, 7, 1);
        g.drawLine(7, 1, 7, 0);
        g.drawLine(7, 0, 6, 0);
        if (firstY) {
            g.drawLine(6, 0, 5, 0);
        }
        g.drawLine(5, 0, 4, 0);

        //cut upper right
        if (lastX) {
            g.drawLine(11, 0, 11, 3);
        }
        g.drawLine(11, 3, 10, 3);
        g.drawLine(10, 3, 10, 2);
        g.drawLine(10, 2, 9, 2);
        g.drawLine(9, 2, 9, 1);
        g.drawLine(9, 1, 8, 1);
        g.drawLine(8, 1, 8, 0);
        g.drawLine(8, 0, 9, 0);
        if (firstY) {
            g.drawLine(9, 0, 11, 0);
        }

        //cut lower right
        g.drawLine(9, 4, 9, 7);
        if (lastY) {
            g.drawLine(9, 7, 11, 7);
        }
        if (lastX) {
            g.drawLine(11, 7, 11, 5);
        }
        g.drawLine(11, 5, 10, 5);
        g.drawLine(10, 5, 10, 4);
        g.drawLine(10, 4, 9, 4);

        //cut lower
        g.drawLine(3, 5, 3, 6);
        g.drawLine(3, 6, 5, 6);
        g.drawLine(5, 6, 5, 7);
        if (lastY) {
            g.drawLine(5, 7, 6, 7);
        }
        g.drawLine(6, 7, 6, 6);
        g.drawLine(6, 6, 8, 6);
        g.drawLine(8, 6, 8, 5);
        g.drawLine(8, 5, 3, 5);

        //cur lower left
        g.drawLine(2, 4, 2, 7);
        if (lastY) {
            g.drawLine(2, 7, 0, 7);
        }
        if (firstX) {
            g.drawLine(0, 7, 0, 5);
        }
        g.drawLine(0, 5, 1, 5);
        g.drawLine(1, 5, 1, 4);
        g.drawLine(1, 4, 2, 4);

        //left eye
        g.drawLine(3, 2, 3, 3);
        g.drawLine(3, 3, 4, 3);
        g.drawLine(4, 3, 4, 2);
        g.drawLine(4, 2, 3, 2);

        //right eye
        g.drawLine(7, 2, 7, 3);
        g.drawLine(7, 3, 8, 3);
        g.drawLine(8, 3, 8, 2);
        g.drawLine(8, 2, 7, 2);

    }
}
