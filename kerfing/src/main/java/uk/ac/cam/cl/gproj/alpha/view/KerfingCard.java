/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.cam.cl.gproj.alpha.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;
import uk.ac.cam.cl.gproj.alpha.interfaces.Box;
import uk.ac.cam.cl.gproj.alpha.interfaces.ChoiceProperty;
import uk.ac.cam.cl.gproj.alpha.interfaces.KerfGenerator;
import uk.ac.cam.cl.gproj.alpha.interfaces.Property;


public class KerfingCard extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private final WizardCard parent;
	private Box box;
	private ChoiceProperty kerfingChoice;
	
	private JPanel patternPanel= new JPanel(new GridLayout(0,1));
	private final JPanel propertyPanel = new JPanel(new GridLayout(0,2));
	private final JPanel navigationPanel = new JPanel(new BorderLayout());
	private final JLabel iconLabel = new JLabel (new ImageIcon());
	private final JPanel iconPanel = new JPanel(new BorderLayout());
	private final JButton next = new JButton("Next");
	private final JButton back = new JButton("Back");
	private final ButtonGroup kerfingChoiceGroup = new ButtonGroup ();
	private final JLabel pliabilityLabel = new JLabel("Pliability:    ");
	private final JLabel tensileStrengthLabel = new JLabel("Tensile Strength:    ");
	private final JLabel normalStrengthLabel = new JLabel("Normal Strength:    ");
	private final JLabel torsonalStrengthLabel = new JLabel("Torsonal Strength:    ");
	private final JLabel pliabilityContentLabel = new JLabel();
	private final JLabel tensileStrengthContentLabel = new JLabel();
	private final JLabel normalStrengthContentLabel = new JLabel();
	private final JLabel torsonalStrengthContentLabel = new JLabel();
	
	private List<KerfGenerator> kerfGeneratorList;
	private ArrayList<String> buttonIdList = new ArrayList<String> ();
	
	public KerfingCard(WizardCard wc){
		setLayout(new BorderLayout());
		this.parent = wc;
		
        propertyPanel.add(pliabilityLabel);
        propertyPanel.add(pliabilityContentLabel);
        propertyPanel.add(tensileStrengthLabel);
        propertyPanel.add(tensileStrengthContentLabel);
        propertyPanel.add(torsonalStrengthLabel);
        propertyPanel.add(torsonalStrengthContentLabel);
        propertyPanel.add(normalStrengthLabel);
        propertyPanel.add(normalStrengthContentLabel);

		
        iconPanel.add(iconLabel, BorderLayout.CENTER);
        iconPanel.add(propertyPanel, BorderLayout.EAST);
        iconPanel.setBorder(BorderFactory.createTitledBorder(""));
        
        next.addActionListener(this);
        back.addActionListener(this);
		navigationPanel.add(back, BorderLayout.WEST);
		navigationPanel.add(next, BorderLayout.EAST);
		
		add(iconPanel, BorderLayout.CENTER);
		add(navigationPanel, BorderLayout.SOUTH);
		
//		dashed.addActionListener(this);
//		herringbone.addActionListener(this);
//		diagonalCross.addActionListener(this);
//		alienInvaders.addActionListener(this);
//		oldMidevilCross.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== next) {
			((CardLayout) parent.getLayout()).next(this.getParent());
		} else if (e.getSource() == back) {
			((CardLayout) parent.getLayout()).previous(this.getParent());
		} else {
		    // then the ActionEvent source must be one of the JRadioButtons from the kerfingChoiceGroup
		    String s = kerfingChoiceGroup.getSelection().toString();  
		    String[] key = s.split("[@]");  
		    
		    // Find out which button was clicked
		    for(int i= 0; i< buttonIdList.size(); i++){  
		        String w = (String)buttonIdList.get(i);  
		        if (key[1].equals(w)){  
		            try {
                        kerfingChoice.setValue(i);
                    } catch (InvalidArgumentException e1) {
                        System.err.println("Choice is not one of the available choices.");
                    }
		        }  
		    } 
		}
		
//		if (e.getSource() == dashed){
//			kerfingChoice.resetChoice();
//			try {
//				kerfingChoice.addChoice(0);
//			} catch (InvalidArgumentException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			
//			pliabilityContentLabel.setText("very good");
//			tensileStrengthContentLabel.setText("good");
//			torsonalStrengthContentLabel.setText("good");
//			normalStrengthContentLabel.setText("very good");		
//		} else if (e.getSource() == herringbone){
//			iconLabel.setIcon(new ImageIcon("resources/kerf-herringbone.jpg"));
//			pliabilityContentLabel.setText(" good");
//			tensileStrengthContentLabel.setText("very good");
//			torsonalStrengthContentLabel.setText("very good");
//			normalStrengthContentLabel.setText("very good");	
//		} else if (e.getSource() == diagonalCross){
//			iconLabel.setIcon(new ImageIcon("resources/kerf-diagcross.jpg"));
//			//propertyLabel.setText("<html>pliability: poor <br> tensile strength: good <br> torsonal strength: fair <br> normal strength: good");
//			pliabilityContentLabel.setText("poor");
//			tensileStrengthContentLabel.setText("good");
//			torsonalStrengthContentLabel.setText("fair");
//			normalStrengthContentLabel.setText("good");	
//		} else if (e.getSource() == alienInvaders){
//			iconLabel.setIcon(new ImageIcon("resources/kerf-alieninvader.jpg"));
//			pliabilityContentLabel.setText("poor");
//			tensileStrengthContentLabel.setText("good");
//			torsonalStrengthContentLabel.setText("fair");
//			normalStrengthContentLabel.setText("good");	
//		} else if (e.getSource() == oldMidevilCross ){
//			iconLabel.setIcon(new ImageIcon("resources/kerf-oldmidevilcross.jpg"));
//			pliabilityContentLabel.setText("fair");
//			tensileStrengthContentLabel.setText("good");
//			torsonalStrengthContentLabel.setText("fair");
//			normalStrengthContentLabel.setText("good");	
//		}
	}

	public void setBox(Box box) {
        this.box = box;
        for (Property p : box.getPropertyList()) {
            if (p instanceof ChoiceProperty) {
                if (p.getName().equals("Kerf Pattern")) {
                    this.remove(patternPanel);
                    patternPanel = new JPanel(new GridLayout(0,1));
                    
                    kerfingChoice = (ChoiceProperty) p;
                    kerfGeneratorList = kerfingChoice.getChoiceList();
                    
                    int genNumber = 0;
                    for (final KerfGenerator k: kerfGeneratorList) {
                    	final int genNumberFinal = genNumber++;
                        JRadioButton kerfButton = new JRadioButton(k.getName());
                        String parsed = kerfButton.getModel().toString();  
                        String[] split = parsed.split("[@]");  
                        buttonIdList.add(split[1]);
                        
                        kerfButton.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                iconLabel.setIcon(new ImageIcon(k.getIcon()));
                                pliabilityContentLabel.setText(k.getPliability());
                                tensileStrengthContentLabel.setText(k.getTensileStrength());
                                normalStrengthContentLabel.setText(k.getNormalStrength());
                                torsonalStrengthContentLabel.setText(k.getTorsonalStrength());
                                try {
                                	kerfingChoice.setValue(genNumberFinal);
                                } catch (InvalidArgumentException e1) {
                                	e1.printStackTrace(); //Not much we can do
                                }
                            }
                        });
                        kerfingChoiceGroup.add(kerfButton);
                        patternPanel.add(kerfButton);
                    }
                    patternPanel.setBorder(BorderFactory.createTitledBorder(""));
                    this.add(patternPanel, BorderLayout.WEST);
                    this.validate();
                    
                    break;
                }
            }
        }
    }
}
