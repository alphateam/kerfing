/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import uk.ac.cam.cl.gproj.alpha.Constants;
import uk.ac.cam.cl.gproj.alpha.exceptions.LayoutException;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * A GraphicsPrinter keeps track of the various sheets a Box needs to cut and outputs them to the desired format.
 *
 * These sheets are modeled as a collection of Graphics contexts, all sharing the same coordinate space.
 *
 * An example of using it:
 * <pre>
 *     {@code
 *     GraphicsPrinter gp = ...;
 *
 *     // Start a sheet
 *     Sheet sheet = gp.getNewSheet();
 *
 *     // Set the size of the sheet
 *     sheet.setBoundingShape(new Rectangle(0, 0, 10, 10));
 *
 *     // Get a context to draw in
 *     SheetContext firstContext = sheet.getNewContext();
 *
 *     // Draw some lines
 *     firstContext.drawLine(1, 1, 1, 9); // Call this a
 *     firstContext.drawLine(3, 1, 3, 9); // Call this b
 *
 *     // Get another context to draw in
 *     SheetContext secondContext = sheet.getNewContext();
 *
 *     // Draw some other lines
 *     secondContext.drawLine(1, 1, 9, 1); // Call this c
 *     secondContext.drawLine(1, 3, 3, 3); // Call this d
 *
 *     // Lay the sheets on material
 *     gp.layOut();
 *
 *     // Print. Lines c and d will be cut after lines a and b
 *     gp.print(new DXFGraphics2D());
 *     }
 * </pre>
 */
public class GraphicsPrinter implements Constants {

    List<Sheet> sheetList;

    /**
     * Create a new GraphicsPrinter
     */
    public GraphicsPrinter() {
        sheetList = new ArrayList<Sheet>();
    }

    /**
     * Begins a new sheet.
     *
     * @return The new sheet
     */
    public Sheet getNewSheet() {
        Sheet newSheet = new StandardSheet();

        sheetList.add(newSheet);

        return newSheet;
    }

    /**
     * Outputs all the sheets.
     *
     * After calling, don't forget to do ExecutableGraphics2D.finalizeWrite() if writing to an ExecutableGraphics2D.
     */
    public void print(Graphics2D graphics2D) {
        for (Sheet sheet : sheetList) {
            sheet.print(graphics2D);
        }
    }

    /**
     * Lay the sheets on physical sheets of material
     *
     * @param   width   physical width of the sheet of plywood
     * @param   height  physical height of the sheet of plywood
     */
    public void layOut(int width, int height) throws LayoutException {
        int x = LAYOUT_MINIMUM_DISTANCE;
        int y = LAYOUT_MINIMUM_DISTANCE;
        int xMax = x;

        for (Sheet sheet : sheetList) {
            Rectangle2D bounds = sheet.getBoundingShape().getBounds2D();
            int sheetWidth = (int)bounds.getWidth();
            int sheetHeight = (int)bounds.getHeight();

            if (y + sheetHeight > height) {
                // Start new column
                x = xMax + LAYOUT_MINIMUM_DISTANCE;
                y = LAYOUT_MINIMUM_DISTANCE;
                xMax = x + sheetWidth;
            }

            if (x + sheetWidth > xMax) {
                xMax = x + sheetWidth;
            }

            if (xMax > width) {
                // TODO: Alternatively, start a new sheet.
                throw new LayoutException("no space left on sheet");
            }

            sheet.translate(x - (int)bounds.getX(), y - (int)bounds.getY());
            y += sheetHeight + LAYOUT_MINIMUM_DISTANCE;
        }
    }

}
