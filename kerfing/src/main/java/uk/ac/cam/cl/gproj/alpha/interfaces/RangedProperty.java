/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;

/**
 * <p>
 *     A generic property that stores a value of any comparable type limited to an inclusive range.
 * </p>
 *
 * <p>
 *     Does not initialise the value, so will be hidden by default.
 * </p>
 *
 * @param <T> The type of the value stored. Has to be comparable, of course.
 */
public abstract class RangedProperty<T extends Comparable<T>> extends Property<T> {

    protected T mMinimum;
    protected T mMaximum;

    /**
     * Constructs a new property with the specified name
     *
     * @param name The property's name
     */
    public RangedProperty(String name) {
        super(name);
    }

    /**
     * Sets the value of the property
     *
     * @param aValue New value
     * @throws InvalidArgumentException If the value is not in the required range
     */
    @Override
    public void setValue(T aValue) throws InvalidArgumentException {
        if ((mMinimum != null && aValue.compareTo(mMinimum) < 0) ||
            (mMaximum != null && mMaximum.compareTo(aValue) < 0)) {
            throw new InvalidArgumentException("Value out of range");
        }

        mValue = aValue;
    }

    /**
     * Get the lower bound of the range
     *
     * @return The lower bound
     */
    public T getMinimum() {
        return mMinimum;
    }

    /**
     * Set the lower bound of the range
     *
     * @param aValue The new value
     */
    public void setMinimum(T aValue) throws InvalidArgumentException {
        if (getValue() != null && getValue().compareTo(aValue) < 0) {
            throw new InvalidArgumentException("New minimum is larger than the value");
        }
        mMinimum = aValue;
    }

    /**
     * Get the upper bound of the range
     *
     * @return The upper bound
     */
    public T getMaximum() {
        return mMaximum;
    }

    /**
     * Set the upper bound of the range
     *
     * @param aValue The new value
     */
    public void setMaximum(T aValue) throws InvalidArgumentException {
        if (getValue() != null && aValue.compareTo(getValue()) < 0) {
            throw new InvalidArgumentException("New maximum is smaller than the value");
        }
        mMaximum = aValue;
    }

}
