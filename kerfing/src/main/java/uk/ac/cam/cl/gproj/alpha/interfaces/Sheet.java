/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a sheet - a collection of cuts that have to be in the same relative position and with no other cuts within
 * its clipping shape (which may not be a rectangle).
 *
 * The coordinates are measured in tenths of millimeters (0.1 mm).
 */
public abstract class Sheet {

    protected List<SheetContext> contextList;

    private Shape mBoundingShape;

    /**
     * Initialises a new Sheet. Nothing special
     */
    protected Sheet() {
        contextList = new ArrayList<SheetContext>();
    }

    // Context methods

    /**
     * Returns a new colour for a new graphics context. This colour must be cut after the ones created before it.
     *
     * @return A new colour
     */
    protected abstract Color getNewColor();

    /**
     * Creates a new drawing context in the sheet's coordinate system, assigns it a colour and returns it. The contents
     * of this new drawing context will be cut after all the ones created before it.
     *
     * @return A new graphics context
     */
    public SheetContext getNewContext() {
        SheetContext context = new SheetContext(getNewColor());
        contextList.add(context);
        return context;
    }

    /**
     * Print the current sheet using the specified Graphics2D renderer.
     *
     * @param graphics The renderer to use.
     */
    public void print(Graphics2D graphics) {
        for (SheetContext context : contextList) {
            context.print(graphics);
        }
    }

    // Sheet manipulation methods

    /**
     * Gets the bounding shape. The bounding shape is a guide for the layout on material. There is no logic to restrict
     * drawing to this shape.
     *
     * @return The current bounding shape or null if none has been set.
     */
    public Shape getBoundingShape() {
        return mBoundingShape;
    }

    /**
     * Sets the bounding shape. The bounding shape is a guide for the layout on material. There is no logic to restrict
     * drawing to this shape.
     *
     * @param newBoundingShape The new bounding shape.
     */
    public void setBoundingShape(Shape newBoundingShape) {
        mBoundingShape = newBoundingShape;
    }

    /**
     * Translates the whole sheet by (x,y)
     *
     * @param x How much to add to the x coordinate
     * @param y How much to add to the y coordinate
     */
    public void translate(int x, int y) {
        for (SheetContext context : contextList) {
            context.translate(x,y);
        }
    }

}
