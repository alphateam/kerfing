/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

/**
  * Define DXF constants used to create DXF files.
  */
public interface DXFConstants {

    // DXF_INTRO and DXF_OUTRO assume that the output file will only
    // have one section, and that this is the "ENTITIES" section.
    String DXF_INTRO = "0\n  SECTION\n2\n  ENTITIES\n";
    String DXF_OUTRO = "0\n  ENDSEC\n0\n  EOF";
    String DXF_ARC = "ARC";
    String DXF_LINE = "LINE";
    String DXF_NEWLINE = "\n";

    int DXF_ENTITY = 0;

    int DXF_COLOR = 62;

    int DXF_LINE_START_X = 10;
    int DXF_LINE_START_Y = 20;
    int DXF_LINE_END_X = 11;
    int DXF_LINE_END_Y = 21;

    int DXF_ARC_CENTER_X = 10;
    int DXF_ARC_CENTER_Y = 20;
    int DXF_ARC_RADIUS = 40;
    int DXF_ARC_ANGLE_START = 50;
    int DXF_ARC_ANGLE_END = 51;
}
