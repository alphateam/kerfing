package uk.ac.cam.cl.gproj.alpha.mid;

import java.util.List;
import java.util.Random;
import java.awt.*;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;

/**
 * A DrawContext that takes multiple DrawContexts in its constructor and 
 * maps all calls to a random subcontext. 
 */
public class RandomPickSheetContext implements DrawContext {

    private List<? extends DrawContext> contexts;
    private Random random;

    /**
     * Creates a new context that randomizes the target DrawContext of a draw call.  
     *
     * @param subcontexts The contexts to pick from
     */
    public RandomPickSheetContext(List<? extends DrawContext> subcontexts) {
        contexts = subcontexts;
        random = new Random();
    }

    /**
     * Draws a line in one of the subcontexts. 
     *
     * @param x1 The first point's x coordinate.
     * @param y1 The first point's y coordinate.
     * @param x2 The second point's x coordinate.
     * @param y2 The second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        contexts.get(random.nextInt(contexts.size())).drawLine(x1, y1, x2, y2);
    }
    
    /**
     * Draws an arc in one of the subcontexts. 
     *
     * For a description of the parameters, see {@link java.awt.Graphics2D#drawArc(int,int,int,int,int,int)}.
     */
    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        contexts.get(random.nextInt(contexts.size())).drawArc(x, y, width, height, startAngle, arcAngle);
    }

}
