package uk.ac.cam.cl.gproj.alpha.mid;

import java.awt.*;
import uk.ac.cam.cl.gproj.alpha.interfaces.*;

/**
 * A DrawContext that takes another DrawContext in its constructor and 
 * maps all calls to this subcontext, after flipping coordinates. 
 */
public class CoordinateFlipper implements DrawContext {

    private DrawContext innerContext;

    /**
     * Creates a new context that switches x and y coordinates. 
     *
     * @param subContext The context to map calls to
     */
    public CoordinateFlipper(DrawContext subContext) {
        innerContext = subContext;
    }

    /**
     * Draws a line in the subcontext after switching axis. 
     *
     * @param x1 The first point's x coordinate.
     * @param y1 The first point's y coordinate.
     * @param x2 The second point's x coordinate.
     * @param y2 The second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        innerContext.drawLine(y1, x1, y2, x2);
    }
    
    /**
     * Draws an arc in the subcontext after switching axis. 
     *
     * For a description of the parameters, see {@link java.awt.Graphics2D#drawArc(int,int,int,int,int,int)}.
     */
    public void drawArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        innerContext.drawArc(y, x, height, width, 45 - startAngle, -arcAngle);
    }

}
