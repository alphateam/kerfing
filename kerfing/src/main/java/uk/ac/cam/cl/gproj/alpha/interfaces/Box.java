/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import java.util.List;

/**
 * <p>
 *     Interface between the front-end and the mid-layer.
 * </p>
 *
 * <p>
 *     Represents a 3D box shape. Describes the box to the front-end.
 * </p>
 */
public interface Box {
    
    /**
     * A simple name for the box.
     *
     * @return A textual name for the box.
     */
    public String getName();

    /**
     * Describes the box (to the user, probably).
     *
     * @return A textual description of the box.
     */
    // TODO: Could return a description of the 3D shape that would be rendered and made interactive (e.g. rotatable)
    public String getDescription();

    /**
     * A picture that gives the user an impression of the sort of box it is. 
     *
     * @return A string reference to a visual icon for the box.
     */
    public String getIcon();

    /**
     * Gets a list of all the configurable properties of a box.
     *
     * @return All the configurable properties of a box.
     */
    // TODO: May need to group them when displaying. Should this method return groups of properties (that would contain
    // a group name and a List&lt;Property&gt;, I presume)?
    public List<Property> getPropertyList();

    /**
     * Calculate and output all the cuts necessary to make this box.
     */
    public void process(GraphicsPrinter graphicsPrinter);

}
