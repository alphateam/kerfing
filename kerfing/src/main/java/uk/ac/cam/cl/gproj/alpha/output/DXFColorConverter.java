/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

import java.awt.Color;
import java.util.Arrays;

import uk.ac.cam.cl.gproj.alpha.exceptions.NoACIException;

/**
 * <p>
 * Converts RGB to ACI (AutoCAD Color Index) according to <a
 * href=http://www.isctex.com/acadcolors.php">this</a> table.
 * </p>
 *
 * <p>
 * The table is represented as two arrays of the same length, one
 * containing RGB values and one containing the associated ACI values.
 * </p>
 */
public class DXFColorConverter {

    protected final static int[] RGB_VALUES = {
        0x000000, 0x545454, 0x767676, 0x808080, 0x989898, 0xBBBBBB,
        0xC0C0C0, 0xDDDDDD, 0xFFFFFF, 0x26000A, 0x261318, 0x4D2630,
        0xA65368, 0xFF0040, 0x800020, 0x804050, 0xA60029, 0x4D0013,
        0xFF809F, 0x260000, 0x4D0000, 0x261313, 0x800000, 0xA60000,
        0x4D2626, 0xFF0000, 0x804040, 0xA65353, 0xFF8080, 0xFF9F80,
        0x4D1300, 0xA62900, 0x802000, 0x805040, 0xFF4000, 0xA66853,
        0x4D3026, 0x260A00, 0x261813, 0x4D3926, 0x4D2600, 0xA67C53,
        0xFFBF80, 0x261300, 0x804000, 0xA65300, 0x806040, 0xFF8000,
        0x261D13, 0x262113, 0x4D3900, 0x4D4326, 0xA67C00, 0xA69153,
        0xFFDF80, 0xFFBF00, 0x806000, 0x807040, 0x261D00, 0x262600,
        0x262613, 0x4D4D00, 0x4D4D26, 0x808000, 0x808040, 0xA6A600,
        0xA6A653, 0xFFFF00, 0xFFFF80, 0x1D2600, 0x608000, 0x708040,
        0xBFFF00, 0xDFFF80, 0x7CA600, 0x91A653, 0x434D26, 0x394D00,
        0x212613, 0x1D2613, 0x80FF00, 0x132600, 0x408000, 0x608040,
        0x53A600, 0xBFFF80, 0x7CA653, 0x264D00, 0x394D26, 0x0A2600,
        0x182613, 0x304D26, 0x68A653, 0x40FF00, 0x208000, 0x508040,
        0x29A600, 0x134D00, 0x9FFF80, 0x002600, 0x132613, 0x004D00,
        0x264D26, 0x008000, 0x00A600, 0x408040, 0x53A653, 0x00FF00,
        0x80FF80, 0x80FF9F, 0x004D13, 0x00A629, 0x008020, 0x408050,
        0x00FF40, 0x53A668, 0x264D30, 0x00260A, 0x132618, 0x264D39,
        0x004D26, 0x53A67C, 0x80FFBF, 0x002613, 0x008040, 0x408060,
        0x00A653, 0x00FF80, 0x13261D, 0x132621, 0x004D39, 0x264D43,
        0x00A67C, 0x53A691, 0x80FFDF, 0x00FFBF, 0x008060, 0x408070,
        0x00261D, 0x002626, 0x132626, 0x004D4D, 0x264D4D, 0x008080,
        0x408080, 0x00A6A6, 0x53A6A6, 0x00FFFF, 0x80FFFF, 0x001D26,
        0x006080, 0x407080, 0x00BFFF, 0x80DFFF, 0x007CA6, 0x5391A6,
        0x26434D, 0x00394D, 0x132126, 0x131D26, 0x0080FF, 0x001326,
        0x004080, 0x0053A6, 0x406080, 0x80BFFF, 0x537CA6, 0x00264D,
        0x26394D, 0x000A26, 0x131826, 0x26304D, 0x5368A6, 0x0040FF,
        0x002080, 0x405080, 0x0029A6, 0x00134D, 0x809FFF, 0x000026,
        0x00004D, 0x000080, 0x0000A6, 0x131326, 0x0000FF, 0x26264D,
        0x404080, 0x5353A6, 0x8080FF, 0x9F80FF, 0x13004D, 0x2900A6,
        0x200080, 0x504080, 0x4000FF, 0x6853A6, 0x30264D, 0x0A0026,
        0x181326, 0x39264D, 0x26004D, 0x7C53A6, 0xBF80FF, 0x130026,
        0x400080, 0x5300A6, 0x604080, 0x8000FF, 0x1D1326, 0x211326,
        0x39004D, 0x43264D, 0x7C00A6, 0x9153A6, 0xDF80FF, 0xBF00FF,
        0x600080, 0x704080, 0x1D0026, 0x260026, 0x261326, 0x4D004D,
        0x800080, 0x4D264D, 0xA600A6, 0x804080, 0xFF00FF, 0xA653A6,
        0xFF80FF, 0x26001D, 0x800060, 0x804070, 0xFF00BF, 0xFF80DF,
        0xA6007C, 0xA65391, 0x4D2643, 0x4D0039, 0x261321, 0x26131D,
        0xFF0080, 0x260013, 0x800040, 0xA60053, 0x804060, 0xFF80BF,
        0xA6537C, 0x4D0026, 0x4D2639, 0xFFFFFF, 0xFF0000, 0xFFFF00,
        0x00FF00, 0x00FFFF, 0x0000FF, 0xFF00FF};

    protected final static int[] ACI_VALUES = {
        0, 250, 251, 8, 252, 253, 9, 254, 7, 248, 249, 247, 243, 240,
        244, 245, 242, 246, 241, 18, 16, 19, 14, 12, 17, 1, 15, 13, 11,
        21, 26, 22, 24, 25, 20, 23, 27, 28, 29, 37, 36, 33, 31, 38, 34,
        32, 35, 30, 39, 49, 46, 47, 42, 43, 41, 40, 44, 45, 48, 58, 59,
        56, 57, 54, 55, 52, 53, 2, 51, 68, 64, 65, 60, 61, 62, 63, 67,
        66, 69, 79, 70, 78, 74, 75, 72, 71, 73, 76, 77, 88, 89, 87, 83,
        80, 84, 85, 82, 86, 81, 98, 99, 96, 97, 94, 92, 95, 93, 3, 91,
        101, 106, 102, 104, 105, 100, 103, 107, 108, 109, 117, 116, 113,
        111, 118, 114, 115, 112, 110, 119, 129, 126, 127, 122, 123, 121,
        120, 124, 125, 128, 138, 139, 136, 137, 134, 135, 132, 133, 4,
        131, 148, 144, 145, 140, 141, 142, 143, 147, 146, 149, 159, 150,
        158, 154, 152, 155, 151, 153, 156, 157, 168, 169, 167, 163, 160,
        164, 165, 162, 166, 161, 178, 176, 174, 172, 179, 5, 177, 175,
        173, 171, 181, 186, 182, 184, 185, 180, 183, 187, 188, 189, 197,
        196, 193, 191, 198, 194, 192, 195, 190, 199, 209, 206, 207, 202,
        203, 201, 200, 204, 205, 208, 218, 219, 216, 214, 217, 212, 215,
        6, 213, 211, 228, 224, 225, 220, 221, 222, 223, 227, 226, 229,
        239, 230, 238, 234, 232, 235, 231, 233, 236, 237, 255, 10, 50,
        90, 130, 170, 210};

    /**
     * Convert a Color to ACI. If there is no associated ACI value, throw a
     * NoACIException.
     * @param   color the color to convert.
     */
    public static int colorToAci(Color color) throws NoACIException {
      return rgbToAci(color.getRGB() & 0xFFFFFF);
    }

    /**
     * Convert RBG to ACI. If there is no associated ACI value, throw a
     * NoACIException.
     * @param   rgb the RBG value.
     */
    public static int rgbToAci(int rgb) throws NoACIException {
        for (int index = 0; index < RGB_VALUES.length; index++) {
            if (RGB_VALUES[index] == rgb)
                return ACI_VALUES[index];
        }
        throw new NoACIException(rgb);
    }
}
