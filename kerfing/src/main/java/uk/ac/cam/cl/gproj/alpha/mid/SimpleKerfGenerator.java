/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import java.util.List;

import uk.ac.cam.cl.gproj.alpha.interfaces.SheetContext;
import uk.ac.cam.cl.gproj.alpha.interfaces.KerfGenerator;


/**
 * A simple kerf line generator. 
 * 
 * <p>
 *     The pattern consists of cuts alternating between two cut types (Type 1
 *     and Type 2) that are both parallel to either the horizontal or vertical 
 *     axis, and are all evenly separated in the other axis. Type 1 cuts leave 
 *     a small section of wood in the middle and Type 2 cuts leave a small 
 *     section of wood on the edges. 
 * </p>
 */
public class SimpleKerfGenerator implements KerfGenerator {

    //Slit separation / distance to circle center (s = separation, d = distance, r = ratio)
    private double sdr = 0.2;
    //percentage of wood to cut out from sides for Type 1 cuts
    private double ratioCutOut = 0.35;
    //percentage of wood to leave on sides for Type 2 cuts
    private double ratioLeaveIn = 0.2;

    private static final String Name = "Simple Kerf Generator";
    private static final String Description = "A basic but strong and reliable kerf pattern. ";
    private static final String Icon = "resources/kerf-dash.jpg";
    private static final String PLIABILITY = "Very good";
    private static final String TENSILE_STRENGTH = "Good";
    private static final String TORSONAL_STRENGTH = "Good";
    private static final String NORMAL_STRENGTH = "Very good";

    /**
     * Default Class Constructor.  
     */
    public SimpleKerfGenerator() { }

    /**
     * Class Constructor specifying custom parameters for the generator to work with. 
     *
     * This constructor exists for the case in which the default constants for this generator are
     * not working out for your use case. 
     *
     * @param separationToDistanceRatio A constant to specify the constant of proportionality between 
     * kerf line separation and the distance to the centre of the circle the wood is bending around. 
     * @param leaveInRatio A constant to specify the ratio of how much wood is left on the edges for Type 2 cuts.
     * @param cutOutRatio A constant to specify the ratio of how much wood is cut out from the edges for Type 1 cuts. 
     */
    public SimpleKerfGenerator(double separationToDistanceRatio, double leaveInRatio, double cutOutRatio) { 
        sdr = separationToDistanceRatio;
        ratioLeaveIn = leaveInRatio;
        ratioCutOut = cutOutRatio;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getIcon() {
        return Icon;
    }

    public String getPliability() {
        return PLIABILITY;
    }

    public String getTensileStrength() {
        return TENSILE_STRENGTH;
    }

    public String getTorsonalStrength() {
        return TORSONAL_STRENGTH;
    }

    public String getNormalStrength() {
        return NORMAL_STRENGTH;
    }

    public void drawKerfs(List<SheetContext> contexts, double x1, double y1, double x2, double y2, boolean vertical, double curvePower) {
        double slitSeparation = -1;
        try {
            slitSeparation = sdr / curvePower;
            if (Double.isNaN(slitSeparation)) {
                return; //No lines needed
            }
        }
        catch (ArithmeticException e) {
            return; //No lines needed
        }
        if (y2 < y1) {
            double tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
        if (x2 < x1) {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
        }
        int context = 0;
        if (slitSeparation > 0) {
            if (vertical) {
                double width = x2 - x1; //assert positive
                for (double j = y1; j < y2; j += slitSeparation) {
                    //Type 1 cutting
                    contexts.get(context).drawLine((int)(x1), (int)(j), (int)(x1 + width * ratioCutOut), (int)(j));
                    contexts.get(context).drawLine((int)(x2 - width * ratioCutOut), (int)(j), (int)(x2), (int)(j));
                    context = (context + 1) % contexts.size();

                    j += slitSeparation;
                    if (j < y2) {
                        //Type 2 cutting
                        contexts.get(context).drawLine((int)(x1 + width * ratioLeaveIn), (int)(j), (int)(x2 - width * ratioLeaveIn), (int)(j));
                        context = (context + 1) % contexts.size();
                    }
                }
            }
            else {
                double height = y2 - y1; //assert positive
                for (double i = x1; i < x2; i += slitSeparation) {
                    //Type 1 cutting
                    contexts.get(context).drawLine((int)(i), (int)(y1), (int)(i), (int)(y1 + height * ratioCutOut));
                    contexts.get(context).drawLine((int)(i), (int)(y2 - height * ratioCutOut), (int)(i), (int)(y2));
                    context = (context + 1) % contexts.size();

                    i += slitSeparation;
                    if (i < x2) {
                        //Type 2 cutting
                        contexts.get(context).drawLine((int)(i), (int)(y1 + height * ratioLeaveIn), (int)(i), (int)(y2 - height * ratioLeaveIn));
                        context = (context + 1) % contexts.size();
                    }
                }
            }
        }
    }
}
