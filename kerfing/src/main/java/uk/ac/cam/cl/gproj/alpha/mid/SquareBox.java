/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.interfaces.*;

import java.awt.*;
import java.util.ArrayList;

/**
 * <p>
 *     Represents a box that is a cuboid with curved corners when looked down upon it. 
 * </p>
 */
public class SquareBox extends StandardBox {

    protected static final String NAME = "Square Box";
    protected static final String DESCRIPTION = "A cuboid shaped box with curved sides. ";
    protected static final String ICON_RESOURCE = "resources/cube.png";

    //Property definitions. Values are in mm for ease of use to the 
    //user (they are converted to 0.1mm units at start of process call)
    private final Property<Integer> mWidthProperty = new RangedProperty<Integer>("Width (mm)") {
        {
            mMaximum = 400;
            mValue = 60;
            mMinimum = 50;
        }
    };
    private final Property<Integer> mHeightProperty = new RangedProperty<Integer>("Height (mm)") {
        {
            mMaximum = 400;
            mValue = 60;
            mMinimum = 50;
        }
    };
    private final Property<Integer> mDepthProperty = new RangedProperty<Integer>("Depth (mm)") {
        {
            mMaximum = 400;
            mValue = 40;
            mMinimum = 10;
        }
    };
    private final Property<Integer> mKerfColorsProperty = new RangedProperty<Integer>("Number of colors for kerf patterns") {
        {
            mMaximum = 6;
            mValue = 3;
            mMinimum = 1;
        }
    };
    private final Property<Double> mWoodThicknessProperty = new RangedProperty<Double>("Thickness of Wood (mm)") {
        {
            mMaximum = 10.0;
            mValue = 3.0;
            mMinimum = 0.0; //let the user select 0 if they want to remove wood thickness error cancelling
        }
    };

    public String getName() {
        return NAME;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public String getIcon() {
        return ICON_RESOURCE;
    }

    public SquareBox()
    {
        Properties.add(mWidthProperty);
        Properties.add(mHeightProperty);
        Properties.add(mDepthProperty);
        Properties.add(mKerfColorsProperty);
        Properties.add(mWoodThicknessProperty);
    }

    public void process(GraphicsPrinter gp) {
        //draw data values
        int width = mWidthProperty.getValue() * 10;
        int height = mHeightProperty.getValue() * 10;
        int cornerRadius = 200;
        int depth = mDepthProperty.getValue() * 10;
        int kerfColors = mKerfColorsProperty.getValue();
        double woodThickness = mWoodThicknessProperty.getValue() * 10;

        //computed values
        int straightWidth = width - cornerRadius - cornerRadius;
        int straightHeight = height - cornerRadius - cornerRadius;
        int cornerArcLength = (int)(0.5*Math.PI*((double)cornerRadius + 0.5*woodThickness));
        int length = 2*(straightWidth) + 2*(straightHeight) + 4*cornerArcLength;

        if (gp == null) {
            System.err.println("Square Box could not find a graphics printer. ");
            return;
        }

        // Start a sheet
        Sheet sheet = gp.getNewSheet();

        // Set the size of the sheet
        sheet.setBoundingShape(new Rectangle(0, 0, length, depth));

        // Get kerf contexts to draw to
        ArrayList<SheetContext> kerfContexts = new ArrayList<SheetContext>();
        for (int i = 0; i < kerfColors; ++i) {
            kerfContexts.add(sheet.getNewContext());
        }

        KerfGenerator kerfer = getKerfGenerator();

        double loc = straightWidth / 2;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightHeight;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightWidth;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));
        loc += cornerArcLength + straightHeight;
        kerfer.drawKerfs(kerfContexts, loc, 0.0, loc + (double)cornerArcLength, (double)depth, false, 1.0 / ((double)cornerRadius));

        // Get last context to draw to
        SheetContext lastContext = sheet.getNewContext();

        // Draw boundary lines
        lastContext.drawLine(0, 0, length, 0); //top
        lastContext.drawLine(length, 0, length, depth); //right
        lastContext.drawLine(length, depth, 0, depth); //bottom
        lastContext.drawLine(0, depth, 0, 0); //left

        //Print sides of box
        Sheet front = gp.getNewSheet();
        Sheet back = gp.getNewSheet();
        front.setBoundingShape(new Rectangle(0, 0, width, height));
        back.setBoundingShape(new Rectangle(0, 0, width, height));
        SheetContext frontContext = front.getNewContext();
        SheetContext backContext = back.getNewContext();

        frontContext.drawArc(0, 0, cornerRadius*2, cornerRadius*2, 90, 90);
        frontContext.drawLine(cornerRadius, 0, width - cornerRadius, 0);
        frontContext.drawArc(straightWidth, 0, cornerRadius*2, cornerRadius*2, 0, 90);
        frontContext.drawLine(width, cornerRadius, width, height - cornerRadius);
        frontContext.drawArc(straightWidth, straightHeight, cornerRadius*2, cornerRadius*2, 270, 90);
        frontContext.drawLine(width - cornerRadius, height, cornerRadius, height);
        frontContext.drawArc(0, straightHeight, cornerRadius*2, cornerRadius*2, 180, 90);
        frontContext.drawLine(0, height - cornerRadius, 0, cornerRadius);

        backContext.drawArc(0, 0, cornerRadius*2, cornerRadius*2, 90, 90);
        backContext.drawLine(cornerRadius, 0, width - cornerRadius, 0);
        backContext.drawArc(straightWidth, 0, cornerRadius*2, cornerRadius*2, 0, 90);
        backContext.drawLine(width, cornerRadius, width, height - cornerRadius);
        backContext.drawArc(straightWidth, straightHeight, cornerRadius*2, cornerRadius*2, 270, 90);
        backContext.drawLine(width - cornerRadius, height, cornerRadius, height);
        backContext.drawArc(0, straightHeight, cornerRadius*2, cornerRadius*2, 180, 90);
        backContext.drawLine(0, height - cornerRadius, 0, cornerRadius);
    }

}
