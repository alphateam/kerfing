/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import org.junit.Test;
import uk.ac.cam.cl.gproj.alpha.interfaces.GraphicsPrinter;
import uk.ac.cam.cl.gproj.alpha.interfaces.SheetContext;
import uk.ac.cam.cl.gproj.alpha.interfaces.KerfGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class KerfGeneratorTest {

    /**
     * This tests the SimpleKerfGenerator, by trying to draw a 50x200 patch
     * in the horizontal direction with power 0.005, and kerf parameters
     * separationToDistanceRatio = 0.1, leaveInRatio = 0.2, cutOutRatio = 0.35.
     *
     * These parameters should create 5 cuts. The first two are a Type 1 cut, the
     * third is a Type 2 cut, and the last two are a final Type 1 cut.
     **/
    @Test
    public void simpleKerfGeneratorTest() {
        Set<Line> expectedLines = new TreeSet<Line>();
        expectedLines.add(new Line(0, 0, 0, 70));
        expectedLines.add(new Line(0, 130, 0, 200));
        expectedLines.add(new Line(20, 40, 20, 160));
        expectedLines.add(new Line(40, 0, 40, 70));
        expectedLines.add(new Line(40, 130, 40, 200));

        LineExpectingGraphics2D expectingGraphics2D = new LineExpectingGraphics2D("", expectedLines);

        GraphicsPrinter gp = new GraphicsPrinter();

        List<SheetContext> contexts = new ArrayList<SheetContext>();
        contexts.add(gp.getNewSheet().getNewContext());

        KerfGenerator gen = new SimpleKerfGenerator(0.1, 0.2, 0.35);
        gen.drawKerfs(contexts, 0.0, 0.0, 50.0, 200.0, false, 0.005);

        gp.print(expectingGraphics2D);
        expectingGraphics2D.expectDone();
    }
}
