/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.output;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Unit tests for DXFGraphics2D
 */
public class DXFGraphics2DTest {

    /**
     * Test simple line drawing
     */
    @Test
    public void simpleLine() throws IOException {
        DXFGraphics2D dxfG = new DXFGraphics2D();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        dxfG.drawLine(0, 0, 2, 3);
        dxfG.finalizeWrite(out);

        String expected = "0\n  SECTION\n2\n  ENTITIES\n0\n  LINE\n21\n  -3.0\n20\n  -0.0\n10\n  0.0\n11\n  2.0\n62\n  0\n0\n  ENDSEC\n0\n  EOF";
        String got = out.toString("utf-8").trim();
        
        Assert.assertEquals("failure - line drawing not same",
                expected, got);
    }

    /**
     * Test line drawing with colour
     */
    @Test
    public void coloredLine() throws IOException {
        DXFGraphics2D dxfG = new DXFGraphics2D();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        dxfG.setColor(Color.YELLOW);
        dxfG.drawLine(0, 0, 2, 3);
        dxfG.finalizeWrite(out);

        String expected = "0\n  SECTION\n2\n  ENTITIES\n0\n  LINE\n21\n  -3.0\n20\n  -0.0\n10\n  0.0\n11\n  2.0\n62\n  2\n0\n  ENDSEC\n0\n  EOF";
        String got = out.toString("utf-8").trim();
        
        Assert.assertEquals("failure - colored line drawing not same",
                expected, got);
    }
}

