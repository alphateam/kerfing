/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import junit.framework.Assert;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Expects a given set of lines to be drawn and only them. Ignores color and does not accept multiple identical lines
 */
public class LineExpectingGraphics2D extends Graphics2D {

    private String mName;
    private Set<Line> mExpectedLines;

    /**
     * Expect the following lines to be drawn.
     *
     * @param name A name for this instance
     * @param expectedLines Lines to expect to be drawn
     */
    public LineExpectingGraphics2D(String name, Set<Line> expectedLines) {
        mName = name;
        mExpectedLines = expectedLines;
    }

    /**
     * Convenience constructor that takes an int[] and make a set of lines by drawing lines between adjacent points in
     * the given array.
     *
     * @param name A name for this instance
     * @param points An array of points. X and Y coordinates alternate so if we have 3 points A, B and C the array would
     *               look like: { Ax, Ay, Bx, By, Cx, Cy }
     */
    public LineExpectingGraphics2D(String name, int[] points) {
        this(name, (Set<Line>)null);

        mExpectedLines = new TreeSet<Line>();
        for (int i = 0; i < points.length-3; i += 2) {
            mExpectedLines.add(new Line(points[i], points[i+1], points[i+2], points[i+3]));
        }
    }

    /**
     * Checks if there are any more lines left to draw
     */
    public void expectDone() {
        Assert.assertTrue(mName + ": " + mExpectedLines.size() + " expected lines have not been drawn", mExpectedLines.isEmpty());
    }

    /**
     * Checks if the line drawn was expected
     *
     * @param i See Graphics.drawLine
     * @param i2 See Graphics.drawLine
     * @param i3 See Graphics.drawLine
     * @param i4 See Graphics.drawLine
     */
    @Override
    public void drawLine(int i, int i2, int i3, int i4) {
        Line thisLine = new Line(i, i2, i3, i4);

        // Expect expected line
        Assert.assertTrue(mName + ": " + "Unexpected line drawn: " + thisLine.toString(), mExpectedLines.contains(thisLine));

        // Remove line from set
        mExpectedLines.remove(thisLine);
    }

    /**
     * Ignored
     *
     * @param color Ignored
     */
    @Override
    public void setColor(Color color) {}

    // Fail everything else

    private String unexpectedCallMessage = mName + ": " + "Unexpected call to: ";

    /**
     * Gets the name of the method that calls this
     *
     * @return The name of the method this is called from
     */
    private String getCurrentMethodName() {
        return new Throwable().getStackTrace()[1].toString();
    }

    @Override
    public void draw(Shape shape) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }

    @Override
    public boolean drawImage(Image image, AffineTransform transform, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public void drawImage(BufferedImage bufferedImage, BufferedImageOp bufferedImageOp, int i, int i2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawRenderedImage(RenderedImage renderedImage, AffineTransform transform) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawRenderableImage(RenderableImage renderableImage, AffineTransform transform) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawString(String s, int i, int i2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawString(String s, float v, float v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawString(AttributedCharacterIterator attributedCharacterIterator, int i, int i2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public boolean drawImage(Image image, int i, int i2, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public boolean drawImage(Image image, int i, int i2, int i3, int i4, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public boolean drawImage(Image image, int i, int i2, Color color, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public boolean drawImage(Image image, int i, int i2, int i3, int i4, Color color, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Color color, ImageObserver imageObserver) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public void dispose() { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawString(AttributedCharacterIterator attributedCharacterIterator, float v, float v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawGlyphVector(GlyphVector glyphVector, float v, float v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void fill(Shape shape) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public boolean hit(Rectangle rectangle, Shape shape, boolean b) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return false;
    }

    @Override
    public GraphicsConfiguration getDeviceConfiguration() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void setComposite(Composite composite) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setPaint(Paint paint) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setStroke(Stroke stroke) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setRenderingHint(RenderingHints.Key key, Object o) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public Object getRenderingHint(RenderingHints.Key key) {
        return null;
    }

    @Override
    public void setRenderingHints(Map<?, ?> map) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void addRenderingHints(Map<?, ?> map) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public RenderingHints getRenderingHints() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public Graphics create() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void translate(int i, int i2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public Color getColor() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void setPaintMode() { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setXORMode(Color color) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public Font getFont() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void setFont(Font font) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public FontMetrics getFontMetrics(Font font) {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public Rectangle getClipBounds() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void clipRect(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setClip(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public Shape getClip() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void setClip(Shape shape) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void copyArea(int i, int i2, int i3, int i4, int i5, int i6) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }

    @Override
    public void fillRect(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void clearRect(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawRoundRect(int i, int i2, int i3, int i4, int i5, int i6) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void fillRoundRect(int i, int i2, int i3, int i4, int i5, int i6) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawOval(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void fillOval(int i, int i2, int i3, int i4) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawArc(int i, int i2, int i3, int i4, int i5, int i6) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void fillArc(int i, int i2, int i3, int i4, int i5, int i6) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawPolyline(int[] ints, int[] ints2, int i) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void drawPolygon(int[] ints, int[] ints2, int i) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void fillPolygon(int[] ints, int[] ints2, int i) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void translate(double v, double v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void rotate(double v) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void rotate(double v, double v2, double v3) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void scale(double v, double v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void shear(double v, double v2) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void transform(AffineTransform transform) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public void setTransform(AffineTransform transform) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public AffineTransform getTransform() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public Paint getPaint() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public Composite getComposite() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void setBackground(Color color) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public Color getBackground() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public Stroke getStroke() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }

    @Override
    public void clip(Shape shape) { Assert.fail(unexpectedCallMessage + getCurrentMethodName()); }     

    @Override
    public FontRenderContext getFontRenderContext() {
        Assert.fail(unexpectedCallMessage + getCurrentMethodName());
        return null;
    }
}
