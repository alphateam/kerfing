/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.mid;

import uk.ac.cam.cl.gproj.alpha.Constants;
import uk.ac.cam.cl.gproj.alpha.interfaces.GraphicsPrinter;
import uk.ac.cam.cl.gproj.alpha.interfaces.Sheet;
import uk.ac.cam.cl.gproj.alpha.interfaces.SheetContext;
import uk.ac.cam.cl.gproj.alpha.output.OutputSVGGraphics2D;

import java.awt.*;
import java.io.FileOutputStream;

public abstract class MidTestUtilities {

    /**
     * Output the contents of some GraphicsPrinters to SVGs for debugging purposes.
     *
     * @param gps The GraphicsPrinters to output
     * @param sheets The sheets used in the GraphicsPrinters
     * @param sheetContexts The sheet contexts used in the GraphicsPrinters
     * @param universe A bounding rectangle to set on the sheets
     * @param filename The filename. A timestamp and the index of the GraphicsPrinter in gps will be appended onto this
     */
    public static void OutputDebugSVG(GraphicsPrinter[] gps, Sheet[] sheets, SheetContext[] sheetContexts,
                                      Rectangle universe, String filename) {
        try {
            // Save output in SVG if test fails to make debugging easier :)
            // Of course, don't trust that the SVG output is right, it could be completely wrong
            String timestamp = Long.toString(System.currentTimeMillis());

            // Draw axes
            for (SheetContext sheetContext : sheetContexts) {
                sheetContext.drawLine(universe.x, 0, universe.x + universe.width, 0);
                sheetContext.drawLine(0, universe.y, 0, universe.y + universe.height);
            }

            // Lay out
            int layoutWidth = universe.width + 2 * Constants.LAYOUT_MINIMUM_DISTANCE;
            int layoutHeight = universe.height + 2 * Constants.LAYOUT_MINIMUM_DISTANCE;
            for (Sheet sheet : sheets) {
                sheet.setBoundingShape(universe);
            }
            for (GraphicsPrinter gp : gps) {
                gp.layOut(layoutWidth, layoutHeight);
            }

            // Print
            for (int i = 0; i < gps.length; ++i) {
                GraphicsPrinter gp = gps[i];

                OutputSVGGraphics2D svg = new OutputSVGGraphics2D(layoutWidth, layoutHeight);
                gp.print(svg);

                svg.finalizeWrite(new FileOutputStream(filename + timestamp + "-" + i + ".svg"));
            }
        } catch (Exception debugE) {
            System.err.println("Exception in debug SVG output: " + debugE);
            debugE.printStackTrace(System.err);
        }
    }

}
