/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import uk.ac.cam.cl.gproj.alpha.Constants;
import uk.ac.cam.cl.gproj.alpha.exceptions.LayoutException;
import uk.ac.cam.cl.gproj.alpha.mid.LineExpectingGraphics2D;

import java.awt.*;

public class GraphicsPrinterTest implements Constants {

    @Test
    public void printerNewSheet() {
        GraphicsPrinter gp = new GraphicsPrinter();

        // Check we get different sheets
        Sheet s1 = gp.getNewSheet();
        Sheet s2 = gp.getNewSheet();
        Assert.assertNotSame("GraphicsPrinter.getNewSheet does not return a new sheet", s1, s2);
    }

    // Also covers Sheet.translate and SheetContext.translate
    // Depends on GraphicsPrinter.print
    @Test
    public void printerLayOutNegative() throws LayoutException {
        final int x = -500;
        final int y = -500;
        final int w = 2 * -x;
        final int h = 2 * -y;

        GraphicsPrinter gp = new GraphicsPrinter();

        Sheet sheet = gp.getNewSheet();
        sheet.setBoundingShape(new Rectangle(x, y, w, h));

        SheetContext context = sheet.getNewContext();
        context.drawLine(x, y, x + w, y + h);

        LineExpectingGraphics2D graphics2D = new LineExpectingGraphics2D("Layout test", new int[] {
                LAYOUT_MINIMUM_DISTANCE, LAYOUT_MINIMUM_DISTANCE,
                LAYOUT_MINIMUM_DISTANCE + w, LAYOUT_MINIMUM_DISTANCE + h
        });

        gp.layOut(w + 2 * LAYOUT_MINIMUM_DISTANCE, h + 2 * LAYOUT_MINIMUM_DISTANCE);

        gp.print(graphics2D);
        graphics2D.expectDone();
    }

    // TODO more general printerLayOut

    // Also covers Sheet.print and SheetContext.print
    @Ignore
    @Test
    public void printerPrint() {
        // TODO test GraphicsPrinter.print
    }

    @Test
    public void sheetNewContext() {
        Sheet s = new StandardSheet();

        // Check we get different contexts
        SheetContext c1 = s.getNewContext();
        SheetContext c2 = s.getNewContext();
        Assert.assertNotSame("Sheet.getNewContext does not return a new context", c1, c2);
    }

    @Test
    public void standardSheetGetNewColor() {
        Sheet s = new StandardSheet();

        Color[] expectedColors = {
                Color.black,
                Color.red,
                Color.green,
                Color.yellow,
                Color.blue,
                Color.magenta,
                Color.cyan,
                Color.orange,
                Color.pink
        };

        for (int k = 0; k < 4; ++k) {
            for (Color expectedColor : expectedColors) {
                Color color = s.getNewColor();
                Assert.assertEquals("Expected color: " + expectedColor + ". Got color: " + color,
                        color, expectedColor);
            }
        }
    }

}
