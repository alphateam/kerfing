/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import junit.framework.Assert;
import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates a mock box to be used in BoxTest.
 *
 * Properties: <ul>
 *     <li>boolean</li>
 *     <li>integer</li>
 *     <li>string</li>
 *     <li>ranged integer</li>
 *     <li>single choice</li>
 *     <li>multiple choice</li>
 * </ul>
 *
 * Update BoxTest accordingly after updating this class.
 */
public class MockBox implements Box {

    private List<Property> propertyList;

    public MockBox() {
        // Set constants
        final String booleanName = "test boolean property";
        final Boolean booleanDefault = true;

        final String integerName = "test integer property";
        final Integer integerDefault = 0;

        final String stringName = "test string property";
        final String stringDefault = "string";

        final String integerRangedName = "test integer ranged property";
        final int integerRangedMinimum = 1;
        final int integerRangedMaximum = 10;
        final int integerRangedDefault = 1;

        final String singleChoiceName = "test single choice property";
        final List<String> singleChoiceList = new ArrayList<String>();
        singleChoiceList.add("single choice 0");
        singleChoiceList.add("single choice 1");
        singleChoiceList.add("single choice 2");
        singleChoiceList.add("single choice 3");

        final String multipleChoiceName = "test multiple choice property";
        final List<String> multipleChoiceList = new ArrayList<String>();
        multipleChoiceList.add("multiple choice 0");
        multipleChoiceList.add("multiple choice 1");
        multipleChoiceList.add("multiple choice 2");
        multipleChoiceList.add("multiple choice 3");

        // Build property list
        propertyList = new ArrayList<Property>();

        try {
            // Add a boolean property
            propertyList.add(new GenericProperty<Boolean>(booleanName) {
                {
                    setValue(booleanDefault);
                }
            });

            // Add an integer property
            propertyList.add(new GenericProperty<Integer>(integerName) {
                {
                    setValue(integerDefault);
                }
            });

            // Add a string property
            propertyList.add(new GenericProperty<String>(stringName) {
                {
                    setValue(stringDefault);
                }
            });

            // Add a ranged integer property
            propertyList.add(new RangedProperty<Integer>(integerRangedName) {
                {
                    setMinimum(integerRangedMinimum);
                    setMaximum(integerRangedMaximum);
                    setValue(integerRangedDefault);
                }
            });

            // Add a single choice property
            propertyList.add(new ChoiceProperty<String>(singleChoiceName) {
                @Override
                public List<String> getChoiceList() {
                    return singleChoiceList;
                }
            });

            // Add a multiple choice property
            propertyList.add(new ChoiceProperty<String>(multipleChoiceName) {
                {
                    setMultipleChoice(true);
                }

                @Override
                public List<String> getChoiceList() {
                    return multipleChoiceList;
                }
            });
        } catch (InvalidArgumentException e) {
            Assert.fail("Unreachable: The mock box default property values are wrong");
        }
    }

    public String getDescription() {
        return "mock box";
    }

    public String getName() {
        return "Mock Box";
    }

    public String getIcon() {
        return "resources/cube.png";
    }

    public List<Property> getPropertyList() {
        return propertyList;
    }

    public void process(GraphicsPrinter graphicsPrinter) {
        // Nothing to do here
    }

}
