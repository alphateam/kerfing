/*
 * Copyright (c) 2013, Christopher Cox, Sebastian Funk, Jingyi Luo,
 * Leonhard Markert, Vlad Tataranu and Ranna Zhou.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are
 * met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package uk.ac.cam.cl.gproj.alpha.interfaces;

import junit.framework.Assert;
import org.junit.Test;
import uk.ac.cam.cl.gproj.alpha.exceptions.InvalidArgumentException;

import java.util.Iterator;

public class BoxTest {

    private static final Integer RANGED_MINIMUM = -5;
    private static final Integer RANGED_MAXIMUM = 5;

    private static final String INVALID_ARGUMENTS_MESSAGE = "Expected InvalidArgumentException";

    /**
     * Creates a mock box and makes sure the properties have the right classes
     */
    @Test
    public void reflection() {
        Box mockBox = new MockBox();

        Iterator<Property> propertyIterator = mockBox.getPropertyList().iterator();
        Property p;

        final String classErrorMessage = "Mock box property is not the right class or value is null";

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof GenericProperty);
        Assert.assertTrue(classErrorMessage, p.getValue() instanceof Boolean);

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof GenericProperty);
        Assert.assertTrue(classErrorMessage, p.getValue() instanceof Integer);

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof GenericProperty);
        Assert.assertTrue(classErrorMessage, p.getValue() instanceof String);

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof RangedProperty);
        Assert.assertTrue(classErrorMessage, p.getValue() instanceof Integer);

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof ChoiceProperty);
        Assert.assertTrue(classErrorMessage, !((ChoiceProperty) p).isMultipleChoice());

        p = propertyIterator.next();
        Assert.assertTrue(classErrorMessage, p instanceof ChoiceProperty);
        Assert.assertTrue(classErrorMessage, ((ChoiceProperty) p).isMultipleChoice());
    }

    // Covers Property.getName, Property.getValue
    @Test
    public void genericProperty() throws InvalidArgumentException {
        final String name = "test generic property name";
        final Integer value = 456;

        Property<Integer> property = new GenericProperty<Integer>(name) {
            {
                setValue(value);
            }
        };

        Assert.assertEquals("The property name is wrong", name, property.getName());
        Assert.assertEquals("The property value is wrong", value, property.getValue());
    }

    private void rangedPropertyIntegerOk(final Integer value) throws InvalidArgumentException {
        Property<Integer> property;

        property = new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                setMaximum(RANGED_MAXIMUM);
                setValue(value);
            }
        };
        Assert.assertEquals("The property value is wrong", value, property.getValue());

        property = new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                setValue(value);
                setMaximum(RANGED_MAXIMUM);
            }
        };
        Assert.assertEquals("The property value is wrong", value, property.getValue());

        property = new RangedProperty<Integer>("") {
            {
                setMaximum(RANGED_MAXIMUM);
                setValue(value);
                setMinimum(RANGED_MINIMUM);
            }
        };
        Assert.assertEquals("The property value is wrong", value, property.getValue());

        property = new RangedProperty<Integer>("") {
            {
                setValue(value);
                setMinimum(RANGED_MINIMUM);
                setMaximum(RANGED_MAXIMUM);
            }
        };
        Assert.assertEquals("The property value is wrong", value, property.getValue());
    }

    @Test
    public void rangedPropertyIntegerOk() throws InvalidArgumentException {
        for (int value = RANGED_MINIMUM; value <= RANGED_MAXIMUM; ++value) {
            rangedPropertyIntegerOk(value);
        }
    }

    private void rangedPropertyIntegerFailMinimum(final Integer value) throws InvalidArgumentException {
        new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                setMaximum(RANGED_MAXIMUM);
                try {
                    setValue(value);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
            }
        };

        new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                try {
                    setValue(value);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
                setMaximum(RANGED_MAXIMUM);
            }
        };

        new RangedProperty<Integer>("") {
            {
                setMaximum(RANGED_MAXIMUM);
                setValue(value);
                try {
                    setMinimum(RANGED_MINIMUM);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
            }
        };

        new RangedProperty<Integer>("") {
            {
                setValue(value);
                try {
                    setMinimum(RANGED_MINIMUM);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
                setMaximum(RANGED_MAXIMUM);
            }
        };
    }

    private void rangedPropertyIntegerFailMaximum(final Integer value) throws InvalidArgumentException {
        new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                setMaximum(RANGED_MAXIMUM);
                try {
                    setValue(value);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
            }
        };

        new RangedProperty<Integer>("") {
            {
                setMinimum(RANGED_MINIMUM);
                setValue(value);
                try {
                    setMaximum(RANGED_MAXIMUM);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
            }
        };

        new RangedProperty<Integer>("") {
            {
                setMaximum(RANGED_MAXIMUM);
                try {
                    setValue(value);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
                setMinimum(RANGED_MINIMUM);
            }
        };

        new RangedProperty<Integer>("") {
            {
                setValue(value);
                setMinimum(RANGED_MINIMUM);
                try {
                    setMaximum(RANGED_MAXIMUM);
                    Assert.fail(INVALID_ARGUMENTS_MESSAGE);
                } catch (InvalidArgumentException e) {
                    // Expected
                }
            }
        };
    }

    @Test
    public void rangedPropertyIntegerFail() throws InvalidArgumentException{
        rangedPropertyIntegerFailMinimum(RANGED_MINIMUM - 1);
        rangedPropertyIntegerFailMaximum(RANGED_MAXIMUM + 1);
    }

    // TODO ChoiceProperty tests

}
